package ch.bwe.faa.v1.runner.encryption;

import ch.bwe.faa.cli.parse.CommandLineParser;
import ch.bwe.faa.v1.core.service.ServiceRegistry;

/**
 * Runner for encryption.
 *
 * @author Benjamin Weber
 *
 */
public class EncryptionRunner {

	public static void main(String[] args) {
		EncryptionParameters parameters = new EncryptionParameters();
		CommandLineParser.parse(args, parameters);

		switch (parameters.method) {
		case EncryptionParameters.ENCRYPT_METHOD:
			String encryptString = ServiceRegistry.getEncryptionService().encryptString(parameters.password, parameters.text);
			System.out.println(encryptString);
			break;
		case EncryptionParameters.DECRYPT_METHOD:
			String decryptString = ServiceRegistry.getEncryptionService().decryptString(parameters.password, parameters.text);
			System.out.println(decryptString);
			break;
		default:
			ServiceRegistry.getLogProxy().info(EncryptionRunner.class, "Method {0} not implemented", parameters.method);
			return;
		}
	}
}
