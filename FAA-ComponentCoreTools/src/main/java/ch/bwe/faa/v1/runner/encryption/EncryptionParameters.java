package ch.bwe.faa.v1.runner.encryption;

import ch.bwe.faa.cli.parse.Method;
import ch.bwe.faa.cli.parse.Parameter;

/**
 * Parameters for encryption.
 *
 * @author Benjamin Weber
 *
 */
public class EncryptionParameters {
	public static final String ENCRYPT_METHOD = "encrypt";
	public static final String DECRYPT_METHOD = "decrypt";

	@Parameter
	public String password;

	@Parameter
	public String text;

	@Method
	public String method;

}
