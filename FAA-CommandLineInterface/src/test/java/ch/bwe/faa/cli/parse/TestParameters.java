package ch.bwe.faa.cli.parse;

/**
 * Test parameter object.
 *
 * @author Benjamin Weber
 *
 */
public class TestParameters {

	@Method
	private String method;

	@Parameter
	private String stringParameter;

	@Parameter
	private String dynamicStringParameter;

	@Parameter
	private Integer integerParameter;

	@Parameter
	private boolean booleanParameter;

	public String getMethod() {
		return method;
	}

	public String getStringParameter() {
		return stringParameter;
	}

	public String getDynamicStringParameter() {
		return dynamicStringParameter;
	}

	public Integer getIntegerParameter() {
		return integerParameter;
	}

	public boolean isBooleanParameter() {
		return booleanParameter;
	}

}
