package ch.bwe.faa.cli.parse;

import org.junit.Assert;
import org.junit.Test;

/**
 * Test class to test the parser.
 *
 * @author Benjamin Weber
 *
 */
public class TestClassCommandLineParser {

	@Test
	public void testValid() throws Exception {

		String line = "method -stringParameter=string -DdynamicStringParameter = string2 -integerParameter=15 -booleanParameter = true";
		TestParameters parameters = new TestParameters();

		CommandLineParser.parse(line, parameters);

		Assert.assertEquals("method", parameters.getMethod());
		Assert.assertEquals("string", parameters.getStringParameter());
		Assert.assertEquals("string2", parameters.getDynamicStringParameter());
		Assert.assertEquals(Integer.valueOf(15), parameters.getIntegerParameter());
		Assert.assertEquals(true, parameters.isBooleanParameter());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testTwoMethods() throws Exception {

		String line = "method method -parameter=string";
		TestParametersTwoMethods parameters = new TestParametersTwoMethods();

		CommandLineParser.parse(line, parameters);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testNoParameters() throws Exception {

		String line = "method -stringParameter=string -DdynamicStringParameter = string2 -integerParameter=15 -booleanParameter = true";
		TestParametersNoParameters parameters = new TestParametersNoParameters();

		CommandLineParser.parse(line, parameters);
	}
}
