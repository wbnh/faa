package ch.bwe.faa.cli.parse;

/**
 * Invalid test parameters.
 *
 * @author Benjamin Weber
 *
 */
public class TestParametersTwoMethods {

	@Method
	private String method1;

	@Method
	private String method2;

	@Parameter
	private String parameter;
}
