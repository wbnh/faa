package ch.bwe.faa.cli.parse;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Describes the field which shall contain the method name on the command line.
 * Must not occur more than once.
 *
 * @author Benjamin Weber
 *
 */
@Retention(RUNTIME)
@Target(FIELD)
public @interface Method {

}
