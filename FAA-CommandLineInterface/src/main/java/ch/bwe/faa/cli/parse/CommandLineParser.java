package ch.bwe.faa.cli.parse;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.faa.v1.core.util.AssertUtils;

/**
 * Reflective Parser for command line.
 *
 * @author Benjamin Weber
 *
 */
public class CommandLineParser {
	public static final String KEY_VALUE_SEPARATOR = "=";
	public static final String PARAMETER_PREFIX = "-";
	public static final String[] PARAMETER_TYPES = { "", "D" };

	private static enum ParseStatus {
		DEFAULT, PARSING_PARAMETER, PARSING_PARAMETER_VALUE
	}

	public static void parse(String line, Object parameters) {
		AssertUtils.notNull(line);
		AssertUtils.notNull(parameters);

		parse(line.split(" "), parameters);
	}

	public static void parse(String[] line, Object parameters) {
		AssertUtils.notNull(line);
		AssertUtils.notNull(parameters);

		Map<String, Field> fields = buildParameters(parameters);
		Iterator<String> iterator = Arrays.asList(line).iterator();

		ParseStatus status = ParseStatus.DEFAULT;
		String currentKey = null;
		for (; iterator.hasNext();) {
			String part = iterator.next();

			if (ParseStatus.DEFAULT.equals(status)) {
				if (part.startsWith(PARAMETER_PREFIX)) {
					// parameter

					part = part.substring(1);

					for (String type : PARAMETER_TYPES) {
						if ("".equals(type)) {
							continue;
						}

						if (part.startsWith(type)) {
							part = part.substring(type.length());
						}
					}

					status = ParseStatus.PARSING_PARAMETER;
				} else {
					// method

					setField(null, part, fields, parameters);
					// status stays DEFAULT
				}
			}

			switch (status) {
			case PARSING_PARAMETER:
				if (part.contains(KEY_VALUE_SEPARATOR)) {
					String[] keyvalue = part.split(KEY_VALUE_SEPARATOR, 2);
					setField(keyvalue[0], keyvalue[1], fields, parameters);
					status = ParseStatus.DEFAULT;
				} else {
					currentKey = part;
					AssertUtils.assertEquals(KEY_VALUE_SEPARATOR, iterator.next(),
									"Parameter '" + part + "' is malformed, expected: " + KEY_VALUE_SEPARATOR);
					status = ParseStatus.PARSING_PARAMETER_VALUE;
				}
				break;
			case PARSING_PARAMETER_VALUE:
				setField(currentKey, part, fields, parameters);
				currentKey = null;
				status = ParseStatus.DEFAULT;
				break;
			case DEFAULT:
				// Do nothing - already handled
				break;
			default:
				throw new IllegalStateException("Illegal parsing state");
			}
		}
	}

	private static void setField(String fieldName, String value, Map<String, Field> fields, Object parameters) {
		if (!fields.containsKey(fieldName)) {
			// no such parameter - ignore
			return;
		}

		Field field = fields.get(fieldName);

		try {
			field.setAccessible(true);
			Class<?> type = field.getType();

			if (type.isAssignableFrom(String.class)) {
				field.set(parameters, value);
			} else if (type.isAssignableFrom(Boolean.class) || type.isAssignableFrom(Boolean.TYPE)) {
				field.set(parameters, Boolean.valueOf(value));
			} else if (type.isAssignableFrom(Short.class) || type.isAssignableFrom(Short.TYPE)) {
				field.set(parameters, Short.valueOf(value));
			} else if (type.isAssignableFrom(Integer.class) || type.isAssignableFrom(Integer.TYPE)) {
				field.set(parameters, Integer.valueOf(value));
			} else if (type.isAssignableFrom(Long.class) || type.isAssignableFrom(Long.TYPE)) {
				field.set(parameters, Long.valueOf(value));
			} else if (type.isAssignableFrom(Float.class) || type.isAssignableFrom(Float.TYPE)) {
				field.set(parameters, Float.valueOf(value));
			} else if (type.isAssignableFrom(Double.class) || type.isAssignableFrom(Double.TYPE)) {
				field.set(parameters, Double.valueOf(value));
			}

		} catch (IllegalArgumentException | IllegalAccessException e) {
			ServiceRegistry.getLogProxy().error(CommandLineParser.class, "Could not set parameter on field", e);
			throw new IllegalArgumentException(e);
		}
	}

	private static Map<String, Field> buildParameters(Object parameters) {
		Map<String, Field> paramMap = new HashMap<>();
		Field[] fields = parameters.getClass().getDeclaredFields();

		int parameterCount = 0;
		int methodCount = 0;

		for (Field field : fields) {
			if (field.isAnnotationPresent(Parameter.class)) {
				parameterCount++;
				paramMap.put(field.getName(), field);
			}
			if (field.isAnnotationPresent(Method.class)) {
				methodCount++;
				paramMap.put(null, field);
			}
		}

		if (methodCount > 1) {
			throw new IllegalArgumentException("Parameter object must not have more than one method field");
		}
		if (parameterCount < 1) {
			throw new IllegalArgumentException("Parameter object must have at least one parameter field");
		}

		return paramMap;
	}
}
