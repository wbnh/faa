package ch.bwe.faa.cli.parse;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Describes a field which contains a command line parameter.
 *
 * @author Benjamin Weber
 *
 */
@Retention(RUNTIME)
@Target(FIELD)
public @interface Parameter {

}
