package ch.bwe.faa.v1.core.service.configuration;

/**
 * Identifying properties for configurations.
 *
 * @author Benjamin Weber
 *
 */
public class ConfigurationProperties<T> {

	private Class<T> configuration;
	private Class<?> context;
	private String variantId;
	private String password;

	/**
	 * Constructor handling initialization of mandatory fields.
	 *
	 */
	public ConfigurationProperties(Class<T> configuration) {
		setConfiguration(configuration);
		setContext(configuration);
	}

	public Class<T> getConfiguration() {
		return configuration;
	}

	public ConfigurationProperties<T> setConfiguration(Class<T> configuration) {
		this.configuration = configuration;
		return this;
	}

	public Class<?> getContext() {
		return context;
	}

	public ConfigurationProperties<T> setContext(Class<?> context) {
		this.context = context;
		return this;
	}

	public String getVariantId() {
		return variantId;
	}

	public ConfigurationProperties<T> setVariantId(String variantId) {
		this.variantId = variantId;
		return this;
	}

	public String getPassword() {
		return password;
	}

	public ConfigurationProperties<T> setPassword(String password) {
		this.password = password;
		return this;
	}

}
