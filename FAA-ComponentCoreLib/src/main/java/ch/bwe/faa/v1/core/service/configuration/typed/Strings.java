package ch.bwe.faa.v1.core.service.configuration.typed;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.LinkedList;
import java.util.List;

import ch.bwe.faa.v1.core.service.ServiceRegistry;

/**
 * @author benjamin
 *
 */
public interface Strings {

	@SuppressWarnings("unchecked")
	default <T> T getByMethodName(String methodName, Object... arguments) {
		List<Class<?>> paramTypes = new LinkedList<>();
		for (Object currentArg : arguments) {
			if (currentArg != null) {
				paramTypes.add(currentArg.getClass());
			}
		}
		try {
			Method method = getClass().getMethod(methodName, paramTypes.toArray(new Class<?>[paramTypes.size()]));

			Object result = method.invoke(this, arguments);
			Class<?> returnType = method.getReturnType();

			if (returnType.isAssignableFrom(result.getClass())) {
				return (T) result;
			}

		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
						| InvocationTargetException problem) {
			ServiceRegistry.getLogProxy().debug(this, "Could not load method {0}, ignoring", methodName);
		}

		return null;
	}
}
