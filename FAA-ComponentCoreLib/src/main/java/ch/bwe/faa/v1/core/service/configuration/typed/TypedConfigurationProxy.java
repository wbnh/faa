package ch.bwe.faa.v1.core.service.configuration.typed;

import java.io.Serializable;

import ch.bwe.faa.v1.core.service.configuration.ConfigurationProperties;

/**
 * @author benjamin
 *
 */
public interface TypedConfigurationProxy extends Serializable {

	/**
	 * Gets the configuration for the passed context. Loads it if it is not loaded
	 * yet.
	 * 
	 * @param config
	 *          the context to load the configuration for
	 * @deprecated Use ConfigurationProperties
	 */
	@Deprecated
	<T> T get(Class<T> config);

	/**
	 * Gets the configuration for the passed context. Loads it if it is not loaded
	 * yet.
	 * 
	 * @param config
	 *          The name-giving configuration class
	 * @param context
	 *          the context to load the configuration for
	 * @deprecated Use ConfigurationProperties
	 */
	@Deprecated
	<T, U> T get(Class<T> config, Class<U> context);

	/**
	 * Gets the configuration for the passed context. Loads it if it is not loaded
	 * yet.
	 * 
	 * @param config
	 *          the context to load the configuration for
	 * @param variantId
	 *          the variant to load
	 * @deprecated Use ConfigurationProperties
	 */
	@Deprecated
	<T> T get(Class<T> config, String variantId);

	/**
	 * Gets the configuration for the passed context. Loads it if it is not loaded
	 * yet.
	 * 
	 * @param config
	 *          The name-giving configuration class
	 * @param context
	 *          the context to load the configuration for
	 * @param variantId
	 *          the variant to load
	 * @deprecated Use ConfigurationProperties
	 */
	@Deprecated
	<T, U> T get(Class<T> config, Class<U> context, String variantId);

	/**
	 * Gets the configuration for the passed context. Loads it if it is not loaded
	 * yet.
	 * 
	 * @param properties
	 *          The properties of the configuration to load.
	 */
	<T> T get(ConfigurationProperties<T> properties);
}
