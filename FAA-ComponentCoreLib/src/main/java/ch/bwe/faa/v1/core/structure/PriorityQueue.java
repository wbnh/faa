package ch.bwe.faa.v1.core.structure;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Queue;

/**
 * Queue that sorts the elements based on a priority.
 *
 * @author Benjamin Weber
 *
 */
public class PriorityQueue<E> implements Queue<E> {

	private static class PriorityQueueElement<E> {
		private E element;
		private int priority;

		/**
		 * Constructor handling initialization of mandatory fields.
		 *
		 */
		public PriorityQueueElement(E element, int priority) {
			this.element = element;
			this.priority = priority;
		}

		public E getElement() {
			return element;
		}

		public int getPriority() {
			return priority;
		}

		public void setPriority(int priority) {
			this.priority = priority;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public String toString() {
			StringBuilder builder = new StringBuilder();

			builder.append("{");
			builder.append(element);
			builder.append(": ");
			builder.append(priority);
			builder.append("}");

			return builder.toString();
		}
	}

	public static final int NO_PRIORITY = -1;

	private LinkedList<PriorityQueueElement<E>> queue = new LinkedList<>();

	/**
	 * Constructor handling initialization of mandatory fields.
	 *
	 */
	public PriorityQueue() {
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean addAll(Collection<? extends E> c) {
		boolean changed = false;

		if (c == null) {
			return changed;
		}

		for (E e : c) {
			changed |= add(e);
		}
		return changed;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void clear() {
		queue.clear();
	}

	private PriorityQueueElement<E> findElement(E element) {
		if (element == null) {
			return null;
		}

		for (PriorityQueueElement<E> e : queue) {
			if (Objects.equals(element, e.getElement())) {
				return e;
			}
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean contains(Object o) {
		@SuppressWarnings("unchecked")
		PriorityQueueElement<E> foundElement = findElement((E) o);
		if (foundElement == null) {
			return false;
		}

		return queue.contains(foundElement);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean containsAll(Collection<?> c) {
		boolean contains = true;
		for (Object o : c) {
			contains &= contains(o);
			if (contains == false) {
				return contains;
			}
		}
		return contains;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isEmpty() {
		return queue.isEmpty();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Iterator<E> iterator() {
		List<E> list = new LinkedList<>();
		for (PriorityQueueElement<E> e : queue) {
			list.add(e.getElement());
		}
		return list.iterator();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean remove(Object o) {
		@SuppressWarnings("unchecked")
		PriorityQueueElement<E> foundElement = findElement((E) o);
		if (foundElement == null) {
			return false;
		}

		boolean result = queue.remove(foundElement);
		sortQueue();
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean removeAll(Collection<?> c) {
		boolean changed = false;
		for (Object o : c) {
			changed |= remove(o);
		}
		sortQueue();
		return changed;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean retainAll(Collection<?> c) {
		List<PriorityQueueElement<E>> retain = new LinkedList<>();
		for (Object o : c) {
			@SuppressWarnings("unchecked")
			PriorityQueueElement<E> foundElement = findElement((E) o);
			if (foundElement != null) {
				retain.add(foundElement);
			}
		}

		boolean result = queue.retainAll(retain);
		sortQueue();
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int size() {
		return queue.size();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object[] toArray() {
		return toArray(new Object[size()]);
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <T> T[] toArray(T[] a) {
		int size = size();
		if (a.length < size) {
			a = Arrays.copyOf(a, size);
		}
		for (int i = 0; i < queue.size(); i++) {
			a[i] = (T) queue.get(i).getElement();
		}
		return a;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean add(E e) {
		return add(e, NO_PRIORITY);
	}

	public boolean add(E e, int priority) {
		if (e == null) {
			throw new NullPointerException();
		}

		boolean result = queue.add(new PriorityQueueElement<E>(e, priority));
		sortQueue();
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public E element() {
		return queue.element().getElement();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean offer(E e) {
		return offer(e, NO_PRIORITY);
	}

	public boolean offer(E e, int priority) {
		if (e == null) {
			throw new NullPointerException();
		}

		boolean result = queue.offer(new PriorityQueueElement<E>(e, priority));
		sortQueue();
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public E peek() {
		return queue.peek().getElement();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public E poll() {
		return queue.poll().getElement();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public E remove() {
		return queue.remove().getElement();
	}

	public void updatePriority(E e, int priority) {
		PriorityQueueElement<E> foundElement = findElement(e);

		if (foundElement == null) {
			return;
		}

		foundElement.setPriority(priority);
		sortQueue();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return queue.toString();
	}

	public int getPriority() {
		return queue.peek().getPriority();
	}

	public int getPriorityOf(E e) {
		return findElement(e).getPriority();
	}

	private void sortQueue() {
		Collections.sort(queue, new Comparator<PriorityQueueElement<E>>() {

			@Override
			public int compare(PriorityQueueElement<E> o1, PriorityQueueElement<E> o2) {
				if (o1 == null) {
					if (o2 == null) {
						return 0;
					} else {
						return 1;
					}
				} else {
					if (o2 == null) {
						return -1;
					} else {
						if (o1.getPriority() == NO_PRIORITY) {
							if (o2.getPriority() == NO_PRIORITY) {
								return 0;
							} else {
								return 1;
							}
						} else {
							if (o2.getPriority() == NO_PRIORITY) {
								return -1;
							} else {
								return Integer.compare(o1.getPriority(), o2.getPriority());
							}
						}
					}
				}
			}
		});
	}

}
