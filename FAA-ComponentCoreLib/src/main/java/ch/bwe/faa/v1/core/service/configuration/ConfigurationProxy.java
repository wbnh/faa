package ch.bwe.faa.v1.core.service.configuration;

import java.io.Serializable;

/**
 * @author benjamin
 *
 */
public interface ConfigurationProxy extends Serializable {

	/**
	 * Loads the configuration for the passed context. Forces a (re-)load in any
	 * case and overwrites the cache.
	 * 
	 * @param config
	 *          the context to load the configuration for
	 * @deprecated Use ConfigurationProperties
	 */
	@Deprecated
	Configuration loadConfiguration(Class<?> config);

	/**
	 * Loads the configuration for the passed context. Forces a (re-)load in any
	 * case and overwrites the cache.
	 * 
	 * @param config
	 *          The name-giving configuration class
	 * @param context
	 *          the context to load the configuration for
	 * @deprecated Use ConfigurationProperties
	 */
	@Deprecated
	Configuration loadConfiguration(Class<?> config, Class<?> context);

	/**
	 * Loads the configuration for the passed context. Forces a (re-)load in any
	 * case and overwrites the cache.
	 * 
	 * @param config
	 *          the context to load the configuration for
	 * @param variantId
	 *          the variant to load
	 * @deprecated Use ConfigurationProperties
	 */
	@Deprecated
	Configuration loadConfiguration(Class<?> config, String variantId);

	/**
	 * Loads the configuration for the passed context. Forces a (re-)load in any
	 * case and overwrites the cache.
	 * 
	 * @param config
	 *          The name-giving configuration class
	 * @param context
	 *          the context to load the configuration for
	 * @param variantId
	 *          the variant to load
	 * @deprecated Use ConfigurationProperties
	 */
	@Deprecated
	Configuration loadConfiguration(Class<?> config, Class<?> context, String variantId);

	/**
	 * Gets the configuration for the passed context. Loads it if it is not loaded
	 * yet.
	 * 
	 * @param config
	 *          the context to load the configuration for
	 * @deprecated Use ConfigurationProperties
	 */
	@Deprecated
	Configuration getConfiguration(Class<?> config);

	/**
	 * Gets the configuration for the passed context. Loads it if it is not loaded
	 * yet.
	 * 
	 * @param config
	 *          The name-giving configuration class
	 * @param context
	 *          the context to load the configuration for
	 * @deprecated Use ConfigurationProperties
	 */
	@Deprecated
	Configuration getConfiguration(Class<?> config, Class<?> context);

	/**
	 * Gets the configuration for the passed context. Loads it if it is not loaded
	 * yet.
	 * 
	 * @param config
	 *          the context to load the configuration for
	 * @param variantId
	 *          the variant to load
	 * @deprecated Use ConfigurationProperties
	 */
	@Deprecated
	Configuration getConfiguration(Class<?> config, String variantId);

	/**
	 * Gets the configuration for the passed context. Loads it if it is not loaded
	 * yet.
	 * 
	 * @param config
	 *          The name-giving configuration class
	 * @param context
	 *          the context to load the configuration for
	 * @param variantId
	 *          the variant to load
	 * @deprecated Use ConfigurationProperties
	 */
	@Deprecated
	Configuration getConfiguration(Class<?> config, Class<?> context, String variantId);

	/**
	 * Loads the configuration for the passed context. Forces a (re-)load in any
	 * case and overwrites the cache.
	 * 
	 * @param properties
	 *          The properties of the configuration to load.
	 */
	<T> Configuration loadConfiguration(ConfigurationProperties<T> properties);

	/**
	 * Gets the configuration for the passed context. Loads it if it is not loaded
	 * yet.
	 * 
	 * @param properties
	 *          The properties of the configuration to load.
	 */
	<T> Configuration getConfiguration(ConfigurationProperties<T> properties);
}
