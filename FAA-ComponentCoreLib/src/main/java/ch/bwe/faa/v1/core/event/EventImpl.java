package ch.bwe.faa.v1.core.event;

/**
 * @author benjamin
 * @param <D> The type of the event data
 */
public class EventImpl<D> extends AbstractEvent<D> {

  /**
   * Constructor.
   */
  public EventImpl() {
  }

  /**
   * Constructor.
   *
   * @param name the name of the event.
   */
  public EventImpl(String name) {
    super(name);
  }

  /**
   * Constructor.
   *
   * @param data the data given to the event.
   */
  public EventImpl(D data) {
    super(data);
  }

  /**
   * Constructor.
   *
   * @param name the name of the event.
   * @param data the data given to the event.
   */
  public EventImpl(String name, D data) {
    super(name, data);
  }

}
