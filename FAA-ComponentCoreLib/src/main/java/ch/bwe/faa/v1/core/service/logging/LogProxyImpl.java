package ch.bwe.faa.v1.core.service.logging;

import java.text.MessageFormat;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author benjamin
 *
 */
public class LogProxyImpl implements LogProxy {

	/**
	 * Constructor.
	 */
	public LogProxyImpl() {
		super();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void log(Level level, Object logger, String message, Object... arguments) {
		log(level, logger, message, null, arguments);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void log(Level level, Object loggerObject, String message, Throwable throwable, Object... arguments) {
		Logger logger;

		if (loggerObject instanceof Class<?>) {
			logger = LogManager.getLogger(loggerObject);
		} else {
			logger = LogManager.getLogger(loggerObject.getClass());
		}

		StringBuilder logMessage = new StringBuilder(MessageFormat.format(message, arguments));

		if (throwable != null) {
			logMessage.append('\n');
			appendThrowable(throwable, logMessage);
		}

		logger.log(level, logMessage.toString());
	}

	private void appendThrowable(Throwable throwable, StringBuilder builder) {
		builder.append(throwable.getClass().getName());
		builder.append(": ");
		builder.append(throwable.getLocalizedMessage());
		builder.append('\n');

		for (StackTraceElement currentElement : throwable.getStackTrace()) {
			builder.append("    at ");
			builder.append(currentElement);
			builder.append('\n');
		}

		if (throwable.getCause() == null || throwable.getCause() == throwable)
			return;

		builder.append("Caused by: ");
		appendThrowable(throwable.getCause(), builder);
	}

}
