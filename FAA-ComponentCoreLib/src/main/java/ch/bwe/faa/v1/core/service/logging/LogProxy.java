package ch.bwe.faa.v1.core.service.logging;

import org.apache.logging.log4j.Level;

import ch.bwe.faa.v1.core.util.AssertUtils;

/**
 * @author benjamin
 *
 */
public interface LogProxy {

	/**
	 * Logs the passed message for the passed logger with level FATAL.
	 * 
	 * @param logger
	 *          the logger
	 * @param message
	 *          the message
	 * @param problem
	 *          the problem that occurred, must not be null
	 * @param arguments
	 *          the arguments for the message
	 */
	default void fatal(Object logger, String message, Throwable problem, Object... arguments) {
		AssertUtils.notNull(problem);

		log(Level.FATAL, logger, message, problem, arguments);
	}

	/**
	 * Logs the passed message for the passed logger with level ERROR.
	 * 
	 * @param logger
	 *          the logger
	 * @param message
	 *          the message
	 * @param problem
	 *          the optional problem that occurred
	 * @param arguments
	 *          the arguments for the message
	 */
	default void error(Object logger, String message, Throwable problem, Object... arguments) {
		log(Level.ERROR, logger, message, problem, arguments);
	}

	/**
	 * Logs the passed message for the passed logger with level WARN.
	 * 
	 * @param logger
	 *          the logger
	 * @param message
	 *          the message
	 * @param problem
	 *          the optional problem that occurred
	 * @param arguments
	 *          the arguments for the message
	 */
	default void warn(Object logger, String message, Throwable problem, Object... arguments) {
		log(Level.WARN, logger, message, problem, arguments);
	}

	/**
	 * Logs the passed message for the passed logger with level INFO.
	 * 
	 * @param logger
	 *          the logger
	 * @param message
	 *          the message
	 * @param arguments
	 *          the arguments for the message
	 */
	default void info(Object logger, String message, Object... arguments) {
		log(Level.INFO, logger, message, arguments);
	}

	/**
	 * Logs the passed message for the passed logger with level DEBUG.
	 * 
	 * @param logger
	 *          the logger
	 * @param message
	 *          the message
	 * @param arguments
	 *          the arguments for the message
	 */
	default void debug(Object logger, String message, Object... arguments) {
		log(Level.DEBUG, logger, message, arguments);
	}

	/**
	 * Logs the passed message for the passed logger with level TRACE.
	 * 
	 * @param logger
	 *          the logger
	 * @param message
	 *          the message
	 * @param arguments
	 *          the arguments for the message
	 */
	default void trace(Object logger, String message, Object... arguments) {
		log(Level.TRACE, logger, message, arguments);
	}

	/**
	 * Logs the passed message for the passed logger with the passed level.
	 * 
	 * @param level
	 *          the level to log
	 * @param logger
	 *          the logger that desires to log
	 * @param message
	 *          the message to log
	 * @param arguments
	 *          the arguments of the message
	 */
	void log(Level level, Object logger, String message, Object... arguments);

	/**
	 * Logs the passed message for the passed logger with the passed level.
	 * 
	 * @param level
	 *          the level to log
	 * @param logger
	 *          the logger that desires to log
	 * @param message
	 *          the message to log
	 * @param throwable
	 *          the optional problem that occurred
	 * @param arguments
	 *          the arguments of the message
	 */
	void log(Level level, Object logger, String message, Throwable throwable, Object... arguments);
}
