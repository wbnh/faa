package ch.bwe.faa.v1.core.service.configuration;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * @author benjamin
 *
 */
public interface Configuration extends Serializable {

	/**
	 * Gets the configuration value as String.
	 * 
	 * @param key
	 *          the key of the configuration
	 * @return the value
	 */
	String getStringValue(String key);

	/**
	 * Gets the configuration value as Boolean.
	 * 
	 * @param key
	 *          the key of the configuration
	 * @return the value
	 */
	Boolean getBooleanValue(String key);

	/**
	 * Gets the configuration value as Short.
	 * 
	 * @param key
	 *          the key of the configuration
	 * @return the value
	 */
	Short getShortValue(String key);

	/**
	 * Gets the configuration value as Integer.
	 * 
	 * @param key
	 *          the key of the configuration
	 * @return the value
	 */
	Integer getIntegerValue(String key);

	/**
	 * Gets the configuration value as Long.
	 * 
	 * @param key
	 *          the key of the configuration
	 * @return the value
	 */
	Long getLongValue(String key);

	/**
	 * Gets the configuration value as Float.
	 * 
	 * @param key
	 *          the key of the configuration
	 * @return the value
	 */
	Float getFloatValue(String key);

	/**
	 * Gets the configuration value as Double.
	 * 
	 * @param key
	 *          the key of the configuration
	 * @return the value
	 */
	Double getDoubleValue(String key);

	/**
	 * Gets the configuration value as String List.
	 * 
	 * @param key
	 *          the key of the configuration
	 * @return the value
	 */
	List<String> getStringList(String key);

	/**
	 * Gets the configuration value as Boolean List.
	 * 
	 * @param key
	 *          the key of the configuration
	 * @return the value
	 */
	List<Boolean> getBooleanList(String key);

	/**
	 * Gets the configuration value as Short List.
	 * 
	 * @param key
	 *          the key of the configuration
	 * @return the value
	 */
	List<Short> getShortList(String key);

	/**
	 * Gets the configuration value as Integer List.
	 * 
	 * @param key
	 *          the key of the configuration
	 * @return the value
	 */
	List<Integer> getIntegerList(String key);

	/**
	 * Gets the configuration value as Long List.
	 * 
	 * @param key
	 *          the key of the configuration
	 * @return the value
	 */
	List<Long> getLongList(String key);

	/**
	 * Gets the configuration value as Float List.
	 * 
	 * @param key
	 *          the key of the configuration
	 * @return the value
	 */
	List<Float> getFloatList(String key);

	/**
	 * Gets the configuration value as Double List.
	 * 
	 * @param key
	 *          the key of the configuration
	 * @return the value
	 */
	List<Double> getDoubleList(String key);

	/**
	 * @return all the keys in the configuration.
	 */
	Set<String> getKeys();
}
