package ch.bwe.faa.v1.core.graph;

import java.math.BigDecimal;
import java.util.Optional;

/**
 * An edge within a graph.
 *
 * @author Benjamin Weber
 *
 */
public interface GraphEdge<K, V> {

	boolean isBiDirectional();

	Optional<GraphNode<K, V>> getSource();

	GraphNode<K, V> getTarget();

	BigDecimal getWeight();
}
