package ch.bwe.faa.v1.core.command;

import ch.bwe.faa.v1.core.util.AssertUtils;

/**
 * Command with a name. To be used with a registry
 *
 * @author Benjamin Weber
 *
 */
public abstract class AbstractNamedCommand implements Command {

	private String name;

	/**
	 * Constructor handling initialization of mandatory fields.
	 * 
	 * @param name
	 *          the command's name
	 */
	protected AbstractNamedCommand(String name) {
		AssertUtils.notNull(name);

		this.name = name;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
}
