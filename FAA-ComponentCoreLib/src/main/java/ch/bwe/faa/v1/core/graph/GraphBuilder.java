package ch.bwe.faa.v1.core.graph;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import ch.bwe.faa.v1.core.util.AssertUtils;

/**
 * Builder for graph.
 *
 * @author Benjamin Weber
 *
 */
public class GraphBuilder<K, V> {

	private boolean undirected = true;
	private List<GraphNodeImpl<K, V>> nodes = new LinkedList<>();

	/**
	 * Constructor handling initialization of mandatory fields.
	 *
	 */
	public GraphBuilder() {
	}

	public GraphBuilder<K, V> directed() {
		undirected = false;
		return this;
	}

	public GraphBuilder<K, V> node(K key) {
		return node(key, null);
	}

	public GraphBuilder<K, V> node(K key, V value) {
		// no double keys
		GraphNodeImpl<K, V> node = findNode(key);
		if (node == null) {
			node = new GraphNodeImpl<>();
			node.setKey(key);
			nodes.add(node);
		}
		node.setValue(value);
		return this;
	}

	public GraphBuilder<K, V> edge(K sourceKey, K targetKey) {
		return edge(sourceKey, targetKey, null);
	}

	public GraphBuilder<K, V> edge(K sourceKey, K targetKey, BigDecimal weight) {
		GraphNodeImpl<K, V> sourceNode = findNode(sourceKey);
		GraphNodeImpl<K, V> targetNode = findNode(targetKey);
		AssertUtils.notNull(sourceNode);
		AssertUtils.notNull(targetNode);

		// no double edges
		if (sourceNode.hasEdge(targetKey, weight)) {
			return this;
		}

		sourceNode.addEdge(new GraphEdgeImpl<>(undirected, sourceNode, targetNode, weight));
		if (undirected) {
			targetNode.addEdge(new GraphEdgeImpl<>(undirected, targetNode, sourceNode, weight));
		}

		return this;
	}

	private GraphNodeImpl<K, V> findNode(K key) {
		for (GraphNodeImpl<K, V> node : nodes) {
			if (Objects.equals(key, node.getKey())) {
				return node;
			}
		}
		return null;
	}

	public Graph<K, V> build() {
		return buildNodeList();
	}

	public Graph<K, V> buildNodeList() {
		return new NodeListGraph<>(nodes);
	}
}
