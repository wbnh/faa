package ch.bwe.faa.v1.core.command;

/**
 * Interface to describe a command.
 * 
 * @author benjamin
 *
 */
@FunctionalInterface
public interface Command {

	/**
	 * Executes the command.
	 */
	void execute();

}
