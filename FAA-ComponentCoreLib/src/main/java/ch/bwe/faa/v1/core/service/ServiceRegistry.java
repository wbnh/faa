package ch.bwe.faa.v1.core.service;

import ch.bwe.faa.v1.core.service.configuration.ConfigurationProxy;
import ch.bwe.faa.v1.core.service.configuration.ConfigurationProxyImpl;
import ch.bwe.faa.v1.core.service.configuration.typed.TypedConfigurationProxy;
import ch.bwe.faa.v1.core.service.configuration.typed.TypedConfigurationProxyImpl;
import ch.bwe.faa.v1.core.service.encryption.EncryptionService;
import ch.bwe.faa.v1.core.service.logging.LogProxy;
import ch.bwe.faa.v1.core.service.logging.LogProxyImpl;
import ch.bwe.faa.v1.core.util.SystemArguments;

/**
 * @author benjamin
 *
 */
public final class ServiceRegistry {

	/**
	 * The log proxy (cached).
	 */
	private static LogProxy logProxy;

	/**
	 * The configuration proxy (cached).
	 */
	private static ConfigurationProxy configurationProxy;

	/**
	 * The typed configuration proxy (cached).
	 */
	private static TypedConfigurationProxy typedConfigurationProxy;

	private static EncryptionService encryptionService;

	/**
	 * 
	 * Constructor.
	 */
	private ServiceRegistry() {
		super();
	}

	/**
	 * Gets the proxy to use for logging.
	 * 
	 * @return the proxy
	 */
	public static LogProxy getLogProxy() {
		if (logProxy == null)
			logProxy = new LogProxyImpl();

		return logProxy;
	}

	/**
	 * Gets the proxy to use for configurations.
	 * 
	 * @return the proxy
	 */
	public static ConfigurationProxy getConfigurationProxy() {
		if (configurationProxy == null)
			configurationProxy = new ConfigurationProxyImpl();
		return configurationProxy;
	}

	/**
	 * Gets the proxy to use for typed configurations.
	 * 
	 * @return the proxy
	 */
	public static TypedConfigurationProxy getTypedConfigurationProxy() {
		if (typedConfigurationProxy == null)
			typedConfigurationProxy = new TypedConfigurationProxyImpl();
		return typedConfigurationProxy;
	}

	/**
	 * @return the encryptionService
	 */
	public static EncryptionService getEncryptionService() {
		if (encryptionService == null)
			encryptionService = new EncryptionService();
		return encryptionService;
	}

	public static Environment getEnvironment() {
		String environment = System.getProperty(SystemArguments.ENVIRONMENT);
		return Environment.valueOf(environment);
	}
}
