package ch.bwe.faa.v1.core.util;

/**
 * @author benjamin
 */
public interface SystemArguments {

    String ENVIRONMENT = "conf.environment";

    String USER = "conf.user";

    String USER_HOME = "user.home";
}
