package ch.bwe.faa.v1.core.util;

import java.util.Objects;

/**
 * Utility class for assertions.
 * 
 * @author benjamin
 */
public class AssertUtils {

  /**
   * Constructor.
   */
  private AssertUtils() {
    super();
  }

  /**
   * The passed string must not be <code>null</code> or <code>""</code>. Throws an
   * {@link AssertionError} if it is.
   * 
   * @param string the string to check.
   * @throws AssertionError if the passed object is <code>null</code> or
   *                        <code>""</code>
   */
  public static void stringNotNullOrEmpty(String string) {
    notNull(string);
    assertNotEquals(string, "");
  }

  /**
   * The passed string must not be <code>null</code> or <code>""</code>. Throws an
   * {@link AssertionError} if it is.
   * 
   * @param string  the string to check.
   * @param message the message to display if the assertion fails
   * @throws AssertionError if the passed object is <code>null</code> or
   *                        <code>""</code>
   */
  public static void stringNotNullOrEmpty(String string, String message) {
    notNull(string, message);
    assertNotEquals(string, "", message);
  }

  /**
   * The passed object must not be <code>null</code>. Throws an
   * {@link AssertionError} if it is.
   * 
   * @param object the object to check.
   * @throws AssertionError if the passed object is <code>null</code>
   */
  public static void notNull(Object object) {
    notNull(object, null);
  }

  /**
   * The passed object must be <code>null</code>. Throws an {@link AssertionError}
   * if it is not.
   * 
   * @param object the object to check.
   * @throws AssertionError if the passed object is not <code>null</code>
   */
  public static void isNull(Object object) {
    isNull(object, null);
  }

  /**
   * The passed boolean must be <code>true</code>. Throws an
   * {@link AssertionError} if it is not.
   * 
   * @param expression the boolean to check
   * @throws AssertionError if the passed object is not <code>true</code>
   */
  public static void assertTrue(boolean expression) {
    assertTrue(expression, null);
  }

  /**
   * The passed boolean must be <code>true</code>. Throws an
   * {@link AssertionError} if it is not.
   * 
   * @param expression the boolean to check
   * @throws AssertionError if the passed object is not <code>true</code>
   */
  public static void assertTrue(Boolean expression) {
    assertTrue(expression, null);
  }

  /**
   * The passed boolean must be <code>false</code>. Throws an
   * {@link AssertionError} if it is not.
   * 
   * @param expression the boolean to check
   * @throws AssertionError if the passed object is not <code>false</code>
   */
  public static void assertFalse(boolean expression) {
    assertFalse(expression, null);
  }

  /**
   * The passed boolean must be <code>false</code>. Throws an
   * {@link AssertionError} if it is not.
   * 
   * @param expression the boolean to check
   * @throws AssertionError if the passed object is not <code>false</code>
   */
  public static void assertFalse(Boolean expression) {
    assertFalse(expression, null);
  }

  /**
   * The passed objects must be equal. Throws an {@link AssertionError} if they
   * are not.
   * 
   * @param first  the first object to check
   * @param second the second object to check
   * @throws AssertionError if the passed objects are not equal
   */
  public static void assertEquals(Object first, Object second) {
    assertEquals(first, second, null);
  }

  /**
   * The passed objects must not be equal. Throws an {@link AssertionError} if
   * they are.
   * 
   * @param first  the first object to check
   * @param second the second object to check
   * @throws AssertionError if the passed objects are equal
   */
  public static void assertNotEquals(Object first, Object second) {
    assertNotEquals(first, second, null);
  }

  /**
   * The passed objects must be the same. Throws an {@link AssertionError} if they
   * are not.
   * 
   * @param first  the first object to check
   * @param second the second object to check
   * @throws AssertionError if the passed objects are not the same
   */
  public static void assertSame(Object first, Object second) {
    assertSame(first, second, null);
  }

  /**
   * The passed objects must not be the same. Throws an {@link AssertionError} if
   * they are.
   * 
   * @param first  the first object to check
   * @param second the second object to check
   * @throws AssertionError if the passed objects are the same
   */
  public static void assertNotSame(Object first, Object second) {
    assertNotSame(first, second, null);
  }

  /**
   * The passed object must not be <code>null</code>. Throws an
   * {@link AssertionError} if it is.
   * 
   * @param object  the object to check.
   * @param message the message to display if the assertion fails
   * @throws AssertionError if the passed object is <code>null</code>
   */
  public static void notNull(Object object, String message) {
    if (object == null) throw new AssertionError(message != null ? message : object);
  }

  /**
   * The passed object must be <code>null</code>. Throws an {@link AssertionError}
   * if it is not.
   * 
   * @param object  the object to check.
   * @param message the message to display if the assertion fails
   * @throws AssertionError if the passed object is not <code>null</code>
   */
  public static void isNull(Object object, String message) {
    if (object != null) throw new AssertionError(message != null ? message : object);
  }

  /**
   * The passed boolean must be <code>true</code>. Throws an
   * {@link AssertionError} if it is not.
   * 
   * @param expression the boolean to check
   * @param message    the message to display if the assertion fails
   * @throws AssertionError if the passed object is not <code>true</code>
   */
  public static void assertTrue(boolean expression, String message) {
    if (!expression) throw new AssertionError(message != null ? message : expression);
  }

  /**
   * The passed boolean must be <code>true</code>. Throws an
   * {@link AssertionError} if it is not.
   * 
   * @param expression the boolean to check
   * @param message    the message to display if the assertion fails
   * @throws AssertionError if the passed object is not <code>true</code>
   */
  public static void assertTrue(Boolean expression, String message) {
    if (!Boolean.TRUE.equals(expression)) throw new AssertionError(message != null ? message : expression);
  }

  /**
   * The passed boolean must be <code>false</code>. Throws an
   * {@link AssertionError} if it is not.
   * 
   * @param expression the boolean to check
   * @param message    the message to display if the assertion fails
   * @throws AssertionError if the passed object is not <code>false</code>
   */
  public static void assertFalse(boolean expression, String message) {
    if (expression) throw new AssertionError(message != null ? message : expression);
  }

  /**
   * The passed boolean must be <code>false</code>. Throws an
   * {@link AssertionError} if it is not.
   * 
   * @param expression the boolean to check
   * @param message    the message to display if the assertion fails
   * @throws AssertionError if the passed object is not <code>false</code>
   */
  public static void assertFalse(Boolean expression, String message) {
    if (!Boolean.FALSE.equals(expression)) throw new AssertionError(message != null ? message : expression);
  }

  /**
   * The passed objects must be equal. Throws an {@link AssertionError} if they
   * are not.
   * 
   * @param first   the first object to check
   * @param second  the second object to check
   * @param message the message to display if the assertion fails
   * @throws AssertionError if the passed objects are not equal
   */
  public static void assertEquals(Object first, Object second, String message) {
    if (!Objects.equals(first, second))
      throw new AssertionError(message != null ? message : (first + " does not equal " + second));
  }

  /**
   * The passed objects must not be equal. Throws an {@link AssertionError} if
   * they are.
   * 
   * @param first   the first object to check
   * @param second  the second object to check
   * @param message the message to display if the assertion fails
   * @throws AssertionError if the passed objects are equal
   */
  public static void assertNotEquals(Object first, Object second, String message) {
    if (Objects.equals(first, second))
      throw new AssertionError(message != null ? message : (first + " equals " + second));
  }

  /**
   * The passed objects must be the same. Throws an {@link AssertionError} if they
   * are not.
   * 
   * @param first   the first object to check
   * @param second  the second object to check
   * @param message the message to display if the assertion fails
   * @throws AssertionError if the passed objects are not the same
   */
  public static void assertSame(Object first, Object second, String message) {
    if (first != second)
      throw new AssertionError(message != null ? message : (first + " is not the same as " + second));
  }

  /**
   * The passed objects must not be the same. Throws an {@link AssertionError} if
   * they are.
   * 
   * @param first   the first object to check
   * @param second  the second object to check
   * @param message the message to display if the assertion fails
   * @throws AssertionError if the passed objects are the same
   */
  public static void assertNotSame(Object first, Object second, String message) {
    if (first == second) throw new AssertionError(message != null ? message : (first + " is the same as " + second));
  }
}
