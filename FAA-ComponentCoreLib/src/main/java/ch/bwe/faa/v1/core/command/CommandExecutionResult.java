package ch.bwe.faa.v1.core.command;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

/**
 * @author benjamin
 *
 */
public class CommandExecutionResult {

	/**
	 * The timestamp of when the execution started.
	 */
	private LocalDateTime executionStartDateTime = LocalDateTime.MIN;

	/**
	 * The timestamp of when the execution ended
	 */
	private LocalDateTime executionFinishedDateTime = LocalDateTime.MIN;

	/**
	 * Whether one or more problems occurred during execution.
	 */
	private boolean problemOccurred = false;

	/**
	 * The problems that occurred during execution.
	 */
	private List<Throwable> occurredProblems = Collections.emptyList();

	/**
	 * Constructor.
	 */
	CommandExecutionResult() {
		super();
	}

	/**
	 * @return the executionStartDateTime
	 */
	public LocalDateTime getExecutionStartDateTime() {
		return executionStartDateTime;
	}

	/**
	 * @param executionStartDateTime
	 *            the executionStartDateTime to set
	 */
	void setExecutionStartDateTime(LocalDateTime executionStartDateTime) {
		this.executionStartDateTime = executionStartDateTime;
	}

	/**
	 * @return the executionFinishedDateTime
	 */
	public LocalDateTime getExecutionFinishedDateTime() {
		return executionFinishedDateTime;
	}

	/**
	 * @param executionFinishedDateTime
	 *            the executionFinishedDateTime to set
	 */
	void setExecutionFinishedDateTime(LocalDateTime executionFinishedDateTime) {
		this.executionFinishedDateTime = executionFinishedDateTime;
	}

	/**
	 * @return the problemOccurred
	 */
	public boolean isProblemOccurred() {
		return problemOccurred;
	}

	/**
	 * @return the occurredProblems
	 */
	public List<Throwable> getOccurredProblems() {
		return occurredProblems;
	}

	/**
	 * @param occurredProblems
	 *            the occurredProblems to set
	 */
	void setOccurredProblems(List<Throwable> occurredProblems) {
		this.occurredProblems = occurredProblems;

		problemOccurred = occurredProblems != null && !occurredProblems.isEmpty();
	}

}
