package ch.bwe.faa.v1.core.graph;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * Default implementation of a node.
 *
 * @author Benjamin Weber
 *
 */
class GraphNodeImpl<K, V> implements GraphNode<K, V> {

	private K key;
	private V value;
	private List<GraphEdge<K, V>> edges = new LinkedList<>();

	/**
	 * Constructor handling initialization of mandatory fields.
	 *
	 */
	public GraphNodeImpl() {
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setKey(K key) {
		this.key = key;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public K getKey() {
		return key;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setValue(V value) {
		this.value = value;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public V getValue() {
		return value;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<GraphEdge<K, V>> getEdges() {
		return edges;
	}

	void addEdge(GraphEdge<K, V> edge) {
		edges.add(edge);
	}

	void addEdges(Collection<GraphEdge<K, V>> edges) {
		this.edges.addAll(edges);
	}

	boolean hasEdge(K targetKey, BigDecimal weight) {
		for (GraphEdge<K, V> edge : edges) {
			if (Objects.equals(targetKey, edge.getTarget().getKey()) && Objects.equals(weight, edge.getWeight())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		if (key == null) {
			return "null";
		}
		return key.toString();
	}
}
