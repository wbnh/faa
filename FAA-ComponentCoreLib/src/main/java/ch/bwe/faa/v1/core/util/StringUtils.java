package ch.bwe.faa.v1.core.util;

import java.util.LinkedList;
import java.util.List;

/**
 * Utility for string operations.
 *
 * @author Benjamin Weber
 */
public class StringUtils {

  /**
   * The default delimiter for list items.
   */
  public static final String DEFAULT_LIST_DELIMITER = ";";

  /**
   * Concatenates the contents of the array with a space (<code>" "</code>)
   * delimiter.
   * 
   * @param array the array to concatenate
   * @return the concatenated string
   */
  public static String stringArrayToString(String... array) {
    return stringArrayToString(" ", array);
  }

  /**
   * Concatenates the contents of the array with the passed delimiter.
   * 
   * @param delimiter the delimiter to use
   * @param array     the array to concatenate
   * @return the concatenated string
   */
  public static String stringArrayToString(String delimiter, String... array) {
    return stringArrayToString(delimiter, 0, array.length, array);
  }

  /**
   * Concatenates the passed portion of the array to a string.
   * 
   * @param delimiter the delimiter to use
   * @param start     the start index (inclusive)
   * @param end       the end index (inclusive)
   * @param array     the array to concatenate
   * @return the concatenated string
   */
  public static String stringArrayToString(String delimiter, int start, int end, String... array) {
    AssertUtils.assertTrue(start >= 0, "Start index must be 0 or greater");
    AssertUtils.assertTrue(end <= array.length, "End index must lie within the array");
    AssertUtils.assertTrue(start <= end, "End cannot be less than start");

    StringBuilder builder = new StringBuilder();

    for (int i = start; i < end; i++) {
      String string = array[i];
      if (builder.length() > 0) {
        builder.append(delimiter);
      }
      builder.append(string);
    }

    return builder.toString();
  }

  /**
   * Converts the passed string to a list of the passed type.
   * 
   * @param clazz the type of the list
   * @param value the value to convert
   * @return the converted list
   */
  public static <T> List<T> convertStringToList(Class<T> clazz, String value) {
    return convertStringToList(clazz, value, DEFAULT_LIST_DELIMITER);
  }

  /**
   * Converts the passed string to a list of the passed type.
   * 
   * @param clazz     the type of the list
   * @param value     the value to convert
   * @param delimiter the delimiter between items to use
   * @return the converted list
   */
  public static <T> List<T> convertStringToList(Class<T> clazz, String value, String delimiter) {
    List<T> list = new LinkedList<>();

    String[] items = value.split(delimiter);
    for (String item : items) {
      list.add(convertStringToType(clazz, item.trim()));
    }

    return list;
  }

  /**
   * Converts the passed string to the passed type.
   * 
   * @param clazz the type to convert to
   * @param value the value to convert
   * @return the converted value
   */
  @SuppressWarnings("unchecked")
  public static <T> T convertStringToType(Class<T> clazz, String value) {
    AssertUtils.notNull(clazz);

    if (Boolean.class.equals(clazz) || Boolean.TYPE.equals(clazz)) { return (T) Boolean.valueOf(value); }
    if (Byte.class.equals(clazz) || Byte.TYPE.equals(clazz)) { return (T) Byte.valueOf(value); }
    if (Short.class.equals(clazz) || Short.TYPE.equals(clazz)) { return (T) Short.valueOf(value); }
    if (Integer.class.equals(clazz) || Integer.TYPE.equals(clazz)) { return (T) Integer.valueOf(value); }
    if (Long.class.equals(clazz) || Long.TYPE.equals(clazz)) { return (T) Long.valueOf(value); }
    if (Float.class.equals(clazz) || Float.TYPE.equals(clazz)) { return (T) Float.valueOf(value); }
    if (Double.class.equals(clazz) || Double.TYPE.equals(clazz)) { return (T) Double.valueOf(value); }
    if (String.class.equals(clazz)) { return (T) value; }

    throw new UnsupportedOperationException(
        "The passed type '" + clazz.getName() + "' is not supported for conversion from string.");
  }

  /**
   * Checks whether the passed string represents a Boolean.
   * 
   * @param value the string
   * @return whether it is of the type
   */
  public static boolean isBoolean(String value) {
    // as Boolean.valueOf() always returns a boolean, we need to work around it.
    return Boolean.TRUE.toString().equals(value) || Boolean.FALSE.toString().equals(value);
  }

  /**
   * Checks whether the passed string represents a Byte.
   * 
   * @param value the string
   * @return whether it is of the type
   */
  public static boolean isByte(String value) {
    try {
      Byte.valueOf(value);
      return true;
    } catch (NumberFormatException e) {
      return false;
    }
  }

  /**
   * Checks whether the passed string represents a Short.
   * 
   * @param value the string
   * @return whether it is of the type
   */
  public static boolean isShort(String value) {
    try {
      Short.valueOf(value);
      return true;
    } catch (NumberFormatException e) {
      return false;
    }
  }

  /**
   * Checks whether the passed string represents a Integer.
   * 
   * @param value the string
   * @return whether it is of the type
   */
  public static boolean isInteger(String value) {
    try {
      Integer.valueOf(value);
      return true;
    } catch (NumberFormatException e) {
      return false;
    }
  }

  /**
   * Checks whether the passed string represents a Long.
   * 
   * @param value the string
   * @return whether it is of the type
   */
  public static boolean isLong(String value) {
    try {
      Long.valueOf(value);
      return true;
    } catch (NumberFormatException e) {
      return false;
    }
  }

  /**
   * Checks whether the passed string represents a Float.
   * 
   * @param value the string
   * @return whether it is of the type
   */
  public static boolean isFloat(String value) {
    try {
      Float.valueOf(value);
      return true;
    } catch (NumberFormatException e) {
      return false;
    }
  }

  /**
   * Checks whether the passed string represents a Double.
   * 
   * @param value the string
   * @return whether it is of the type
   */
  public static boolean isDouble(String value) {
    try {
      Double.valueOf(value);
      return true;
    } catch (NumberFormatException e) {
      return false;
    }
  }

  /**
   * Transforms the passed string to sentence case (first letter upper case)
   * 
   * @param value the value to transform
   * @return the transformed value
   */
  public static String toSentenceCase(String value) {
    if (value == null) return null;
    if (value == "") return "";
    if (value.length() == 1) return value.toUpperCase();

    return value.substring(0, 1).toUpperCase() + value.substring(1);
  }

}
