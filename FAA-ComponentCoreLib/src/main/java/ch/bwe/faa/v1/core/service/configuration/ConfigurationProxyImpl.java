package ch.bwe.faa.v1.core.service.configuration;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.jasypt.properties.EncryptableProperties;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.faa.v1.core.util.Pair;
import ch.bwe.faa.v1.core.util.SystemArguments;

/**
 * @author benjamin
 */
public class ConfigurationProxyImpl implements ConfigurationProxy {

  private static final long serialVersionUID = 4257293693709997541L;

  /**
   * The extension of the file holding the configurations.
   */
  private static final String FILE_EXTENSION = ".properties";

  /**
   * The cache for the configurations.
   */
  private Map<Pair<Class<?>, String>, Configuration> configurations = new HashMap<>();

  /**
   * Constructor.
   */
  public ConfigurationProxyImpl() {
    super();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Configuration loadConfiguration(Class<?> context) {
    return loadConfiguration(context, context);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Configuration loadConfiguration(Class<?> config, Class<?> context) {
    return loadConfiguration(config, context, null);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Configuration loadConfiguration(Class<?> config, String variantId) {
    return loadConfiguration(config, config, variantId);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Configuration loadConfiguration(Class<?> config, Class<?> context, String variantId) {
    return loadConfiguration(new ConfigurationProperties<>(config).setContext(context).setVariantId(variantId));
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public <T> Configuration loadConfiguration(ConfigurationProperties<T> properties) {
    Class<T> config = properties.getConfiguration();
    Class<?> context = properties.getContext();
    String variantId = properties.getVariantId();

    String variant = variantId != null ? "-" + variantId : "";
    Properties loadedProperties;
    if (properties.getPassword() == null) {
      loadedProperties = new Properties();
    } else {
      loadedProperties = new EncryptableProperties(
          ServiceRegistry.getEncryptionService().createPbeStringEncryptor(properties.getPassword()));
    }

    // 5: environment-unspecific, JAR
    Properties fiveProperties = new Properties();
    InputStream fiveStream = context
        .getResourceAsStream(/*
                              * "/" + context.getPackage().getName().replace('.', '/') + '/' +
                              */ config.getSimpleName() + variant + FILE_EXTENSION);
    if (fiveStream != null) {
      try {
        fiveProperties.load(fiveStream);
        fiveStream.close();
      } catch (IOException e) {
        ServiceRegistry.getLogProxy().warn(this, "Could not load environment-unspecific configuration from jar", e);
        fiveProperties = null;
      }
    }

    // 4: environment-specific, JAR
    Properties fourProperties = new Properties();
    InputStream fourStream = context.getResourceAsStream("/" + System.getProperty(SystemArguments.ENVIRONMENT)
        + File.separator + context.getPackage().getName().replace('.', '/') + '/' + config.getSimpleName() + variant
        + FILE_EXTENSION);
    if (fourStream != null) {
      try {
        fourProperties.load(fourStream);
        fourStream.close();
      } catch (IOException e) {
        ServiceRegistry.getLogProxy().warn(this, "Could not load environment-specific configuration from jar", e);
        fourProperties = null;
      }
    }

    // 3: environment-unspecific, UserHome
    Properties threeProperties = new Properties();
    InputStream threeStream = context.getResourceAsStream(System.getProperty(SystemArguments.USER_HOME) + File.separator
        + context.getPackage().getName() + '.' + config.getSimpleName() + variant + FILE_EXTENSION);
    if (threeStream != null) {
      try {
        threeProperties.load(threeStream);
        threeStream.close();
      } catch (IOException e) {
        ServiceRegistry.getLogProxy().warn(this, "Could not load environment-unspecific configuration from user home",
            e);
        threeProperties = null;
      }
    }

    // 2: environment-specific, UserHome
    Properties twoProperties = new Properties();
    InputStream twoStream = context.getResourceAsStream(System.getProperty(SystemArguments.USER_HOME) + File.separator
        + System.getProperty(SystemArguments.ENVIRONMENT) + File.separator + '.' + context.getPackage().getName()
        + config.getSimpleName() + variant + FILE_EXTENSION);
    if (twoStream != null) {
      try {
        twoProperties.load(twoStream);
        twoStream.close();
      } catch (IOException e) {
        ServiceRegistry.getLogProxy().warn(this, "Could not load environment-specific configuration from user home", e);
        twoProperties = null;
      }
    }

    // 1: System properties
    // Not implemented yet

    // merge properties
    if (fiveProperties != null) loadedProperties.putAll(fiveProperties);

    if (fourProperties != null) loadedProperties.putAll(fourProperties);

    if (threeProperties != null) loadedProperties.putAll(threeProperties);

    if (twoProperties != null) loadedProperties.putAll(twoProperties);

    return new ConfigurationImpl(loadedProperties);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Configuration getConfiguration(Class<?> context) {
    return getConfiguration(context, context);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Configuration getConfiguration(Class<?> config, Class<?> context) {
    return getConfiguration(config, context, null);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Configuration getConfiguration(Class<?> config, String variantId) {
    return getConfiguration(config, config, variantId);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Configuration getConfiguration(Class<?> config, Class<?> context, String variantId) {
    return getConfiguration(new ConfigurationProperties<>(config).setContext(context).setVariantId(variantId));
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public <T> Configuration getConfiguration(ConfigurationProperties<T> properties) {
    Class<?> config = properties.getConfiguration();
    Class<?> context = properties.getContext();
    String variantId = properties.getVariantId();

    Pair<Class<?>, String> key = new Pair<>(config, variantId);

    if (!configurations.containsKey(key)) {
      configurations.put(key, loadConfiguration(config, context, variantId));
    }

    return configurations.get(key);
  }

}
