package ch.bwe.faa.v1.core.graph;

import java.math.BigDecimal;
import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * Traversal log in a graph.
 *
 * @author Benjamin Weber
 *
 */
public class GraphTraversal<K, V> {

	private Deque<GraphNode<K, V>> nodes = new LinkedList<>();
	private Deque<GraphEdge<K, V>> edges = new LinkedList<>();

	/**
	 * Constructor handling initialization of mandatory fields.
	 *
	 */
	public GraphTraversal() {
	}

	void addNode(GraphNode<K, V> node) {
		GraphNode<K, V> last = nodes.peekLast();
		GraphEdge<K, V> edge = null;
		if (last != null) {
			List<GraphEdge<K, V>> lastEdges = last.getEdges();
			for (GraphEdge<K, V> currentEdge : lastEdges) {
				if (Objects.equals(currentEdge.getTarget().getKey(), node.getKey())) {
					edge = currentEdge;
					break;
				}
			}
			if (edge == null) {
				throw new IllegalArgumentException("Passed node is not connected to last node");
			}
		}

		nodes.add(node);

		// first edge is always null
		edges.add(edge);
	}

	void addEdge(GraphEdge<K, V> edge) {
		GraphNode<K, V> lastNode = nodes.peekLast();
		if (lastNode != null) {
			List<GraphEdge<K, V>> lastEdges = lastNode.getEdges();
			boolean valid = false;
			for (GraphEdge<K, V> currentEdge : lastEdges) {
				if (Objects.equals(edge, currentEdge)) {
					valid = true;
					break;
				}
			}
			if (!valid) {
				throw new IllegalArgumentException("Passed edge does not come from previous node");
			}
		} else {
			if (edge.isBiDirectional()) {
				nodes.add(edge.getSource().get());
			} else {
				throw new IllegalStateException("No start node is present, please add it first");
			}
		}

		edges.add(edge);
		nodes.add(edge.getTarget());
	}

	public GraphNode<K, V> nextNode() {
		edges.poll();
		return nodes.poll();
	}

	public GraphEdge<K, V> nextEdge() {
		nodes.poll();
		return edges.poll();
	}

	public BigDecimal getRemainingWeight() {
		BigDecimal result = BigDecimal.ZERO;
		boolean hadWeight = false;

		for (GraphEdge<K, V> edge : edges) {
			if (edge == null) {
				continue;
			}
			BigDecimal weight = edge.getWeight();
			if (weight != null) {
				hadWeight = true;
				result = result.add(weight);
			}
		}

		return hadWeight ? result : BigDecimal.valueOf(edges.size());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();

		Iterator<GraphNode<K, V>> nodeIterator = nodes.iterator();
		Iterator<GraphEdge<K, V>> edgeIterator = edges.iterator();

		while (nodeIterator.hasNext()) {
			GraphNode<K, V> node = nodeIterator.next();
			GraphEdge<K, V> edge = edgeIterator.next();

			if (edge != null) {
				BigDecimal weight = edge.getWeight();
				builder.append(" --");
				if (weight != null) {
					builder.append("[");
					builder.append(weight);
					builder.append("]");
				}
				builder.append("-- ");
			}

			builder.append(node.getKey());
		}

		return builder.toString();
	}
}
