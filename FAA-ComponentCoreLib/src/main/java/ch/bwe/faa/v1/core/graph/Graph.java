package ch.bwe.faa.v1.core.graph;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import ch.bwe.faa.v1.core.structure.PriorityQueue;

/**
 * A graph containing multiple edges and nodes.
 *
 * @author Benjamin Weber
 *
 */
public interface Graph<K, V> {

	List<GraphNode<K, V>> getNodes();

	GraphNode<K, V> getNode(K key);

	List<GraphEdge<K, V>> getEdges();

	GraphEdge<K, V> getEdge(K source, K target);

	GraphEdge<K, V> getEdge(K source, K target, BigDecimal weight);

	default GraphTraversal<K, V> dijkstra(K start, K end) {
		PriorityQueue<GraphNode<K, V>> queue = new PriorityQueue<>();
		queue.addAll(getNodes());

		Map<GraphNode<K, V>, GraphNode<K, V>> previousNodes = new HashMap<>();

		queue.updatePriority(getNode(start), 0);

		int distance = 0;
		while (!queue.isEmpty()) {
			distance = queue.getPriority();
			GraphNode<K, V> currentNode = queue.poll();

			List<GraphEdge<K, V>> edges = currentNode.getEdges();
			for (GraphEdge<K, V> edge : edges) {
				GraphNode<K, V> target = edge.getTarget();

				if (queue.contains(target)) {
					int priority = queue.getPriorityOf(target);
					int currentDistance = distance + (edge.getWeight() != null ? edge.getWeight().intValue() : 1);
					if (priority > currentDistance || priority == PriorityQueue.NO_PRIORITY) {
						queue.updatePriority(target, currentDistance);
						previousNodes.put(target, currentNode);
					}
				}
			}
		}

		Stack<GraphNode<K, V>> traversedNodes = new Stack<>();
		GraphNode<K, V> previousNode = getNode(end);
		while (previousNode != null) {
			traversedNodes.push(previousNode);

			previousNode = previousNodes.get(previousNode);
		}

		GraphTraversal<K, V> traversal = new GraphTraversal<>();

		while (!traversedNodes.isEmpty()) {
			traversal.addNode(traversedNodes.pop());
		}

		return traversal;
	}
}
