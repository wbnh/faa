package ch.bwe.faa.v1.core.event;

import java.time.LocalDateTime;

/**
 * Abstract implementation of an event.
 * 
 * @author benjamin
 * @param <D> the type of the data.
 */
public abstract class AbstractEvent<D> implements Event<D> {

  /**
   * The name or string representation.
   */
  private String name;

  /**
   * The data that was given.
   */
  private D data;

  /**
   * The date the event occurred on.
   */
  private final LocalDateTime timestamp;

  /**
   * Constructor.
   */
  protected AbstractEvent() {
    super();

    timestamp = LocalDateTime.now();
  }

  /**
   * Constructor.
   * 
   * @param name The event name
   */
  protected AbstractEvent(String name) {
    this();

    this.name = name;
  }

  /**
   * Constructor.
   * 
   * @param data The data
   */
  protected AbstractEvent(D data) {
    this();

    this.data = data;
  }

  /**
   * Constructor.
   * 
   * @param name The event name
   * @param data The data
   */
  protected AbstractEvent(String name, D data) {
    this();

    this.name = name;
    this.data = data;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getName() {
    return name;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public D getData() {
    return data;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public LocalDateTime getTimestamp() {
    return timestamp;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String toString() {
    return getName();
  }
}
