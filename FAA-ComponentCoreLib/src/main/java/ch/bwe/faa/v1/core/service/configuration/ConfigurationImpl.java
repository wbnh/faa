package ch.bwe.faa.v1.core.service.configuration;

import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.Set;

/**
 * @author benjamin
 *
 */
public class ConfigurationImpl implements Configuration {

	private static final long serialVersionUID = 1349361243216012029L;

	/**
	 * The properties.
	 */
	private Properties properties;

	/**
	 * The delimiter for lists.
	 */
	public static final String LIST_DELIM = ";";

	/**
	 * Constructor.
	 */
	public ConfigurationImpl(Properties values) {
		super();

		properties = values;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getStringValue(String key) {
		return properties.getProperty(key);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Boolean getBooleanValue(String key) {
		String stringValue = getStringValue(key);
		if (stringValue == null)
			return null;
		return Boolean.valueOf(stringValue);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Short getShortValue(String key) {
		String stringValue = getStringValue(key);
		if (stringValue == null)
			return null;
		return Short.valueOf(stringValue);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Integer getIntegerValue(String key) {
		String stringValue = getStringValue(key);
		if (stringValue == null)
			return null;
		return Integer.valueOf(stringValue);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Long getLongValue(String key) {
		String stringValue = getStringValue(key);
		if (stringValue == null)
			return null;
		return Long.valueOf(stringValue);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Float getFloatValue(String key) {
		String stringValue = getStringValue(key);
		if (stringValue == null)
			return null;
		return Float.valueOf(stringValue);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Double getDoubleValue(String key) {
		String stringValue = getStringValue(key);
		if (stringValue == null)
			return null;
		return Double.valueOf(stringValue);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<String> getStringList(String key) {
		String stringValue = getStringValue(key);
		if (stringValue == null || stringValue.isEmpty())
			return null;
		List<String> list = new LinkedList<>();

		Collections.addAll(list, stringValue.split(LIST_DELIM));

		return list;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Boolean> getBooleanList(String key) {
		List<String> stringValue = getStringList(key);
		if (stringValue == null || stringValue.isEmpty())
			return null;
		List<Boolean> list = new LinkedList<>();

		for (String currentValue : stringValue) {
			list.add(Boolean.valueOf(currentValue));
		}

		return list;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Short> getShortList(String key) {
		List<String> stringValue = getStringList(key);
		if (stringValue == null || stringValue.isEmpty())
			return null;
		List<Short> list = new LinkedList<>();

		for (String currentValue : stringValue) {
			list.add(Short.valueOf(currentValue));
		}

		return list;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Integer> getIntegerList(String key) {
		List<String> stringValue = getStringList(key);
		if (stringValue == null || stringValue.isEmpty())
			return null;
		List<Integer> list = new LinkedList<>();

		for (String currentValue : stringValue) {
			list.add(Integer.valueOf(currentValue));
		}

		return list;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Long> getLongList(String key) {
		List<String> stringValue = getStringList(key);
		if (stringValue == null || stringValue.isEmpty())
			return null;
		List<Long> list = new LinkedList<>();

		for (String currentValue : stringValue) {
			list.add(Long.valueOf(currentValue));
		}

		return list;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Float> getFloatList(String key) {
		List<String> stringValue = getStringList(key);
		if (stringValue == null || stringValue.isEmpty())
			return null;
		List<Float> list = new LinkedList<>();

		for (String currentValue : stringValue) {
			list.add(Float.valueOf(currentValue));
		}

		return list;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Double> getDoubleList(String key) {
		List<String> stringValue = getStringList(key);
		if (stringValue == null || stringValue.isEmpty())
			return null;
		List<Double> list = new LinkedList<>();

		for (String currentValue : stringValue) {
			list.add(Double.valueOf(currentValue));
		}

		return list;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Set<String> getKeys() {
		Set<String> keys = new HashSet<>();

		for (Object key : properties.keySet()) {
			keys.add((String) key);
		}

		return keys;
	}

}
