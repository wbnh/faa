package ch.bwe.faa.v1.core.command;

import ch.bwe.faa.v1.core.util.AssertUtils;

/**
 * Wrapper for commands with a name.
 *
 * @author Benjamin Weber
 *
 */
class NamedCommandWrapper extends AbstractNamedCommand {

	private Command wrappedCommand;

	/**
	 * Constructor handling initialization of mandatory fields.
	 *
	 * @param name
	 *          the name.
	 */
	NamedCommandWrapper(String name, Command command) {
		super(name);

		AssertUtils.notNull(command);

		this.wrappedCommand = command;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void execute() {
		wrappedCommand.execute();
	}

}
