package ch.bwe.faa.v1.core.command;

import java.time.LocalDateTime;
import java.util.Arrays;

import ch.bwe.faa.v1.core.service.ServiceRegistry;

/**
 * @author benjamin
 *
 */
public interface CommandExecutor {

	/**
	 * Executes the command.
	 * 
	 * @param command
	 *          the command to execute
	 * @return The result of the execution
	 */
	default CommandExecutionResult executeCommand(Command command) {
		CommandExecutionResult executionResult = new CommandExecutionResult();

		executionResult.setExecutionStartDateTime(LocalDateTime.now());
		try {
			command.execute();
		} catch (Throwable problem) {
			executionResult.setOccurredProblems(Arrays.asList(problem));

			ServiceRegistry.getLogProxy().error(this, "A problem occurred during command execution", problem);
		} finally {
			executionResult.setExecutionFinishedDateTime(LocalDateTime.now());
		} // finally

		return executionResult;
	}
}
