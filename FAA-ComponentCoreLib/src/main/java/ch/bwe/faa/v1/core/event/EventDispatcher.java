package ch.bwe.faa.v1.core.event;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock;

/**
 * Abstract implementation for classes that want to dispatch events.
 * 
 * @author benjamin
 */
public abstract class EventDispatcher {

  /**
   * The registered listeners.
   */
  private Map<String, List<EventListener<?>>> listeners = new HashMap<>();
  private ReentrantReadWriteLock mutex = new ReentrantReadWriteLock();

  /**
   * Constructor.
   */
  public EventDispatcher() {
    super();
  }

  /**
   * Adds a listener for the passed event.
   * 
   * @param event    the event to listen for
   * @param listener the listener to register
   */
  public <D> void registerListener(Event<D> event, EventListener<Event<D>> listener) {
    WriteLock lock = mutex.writeLock();
    lock.lock();
    try {
      if (!listeners.containsKey(event.getName())) {
        listeners.put(event.getName(), new LinkedList<>());
      }

      listeners.get(event.getName()).add(listener);
    } finally {
      lock.unlock();
    }
  }

  /**
   * Removes a listener for the passed event.
   * 
   * @param event    the event not to listen for any more
   * @param listener the listener to remove
   */
  public <D> void unregisterListener(Event<D> event, EventListener<Event<D>> listener) {
    WriteLock lock = mutex.writeLock();
    lock.lock();
    try {
      if (!listeners.containsKey(event.getName())) { return; }

      List<EventListener<?>> aList = listeners.get(event.getName());
      if (!aList.isEmpty()) {
        aList.remove(listener);
      }
      if (aList.isEmpty()) {
        listeners.put(event.getName(), null);
        return;
      }
    } finally {
      lock.unlock();
    }
  }

  /**
   * Fires an event.
   * 
   * @param event the event to fire
   */
  public void fireEvent(Event<?> event) {
    dispatchEvent(event);
  }

  /**
   * Fires an event.
   * 
   * @param event the event to fire
   */
  public <D> void dispatchEvent(Event<D> event) {
    ReadLock lock = mutex.readLock();
    lock.lock();
    try {
      if (listeners.containsKey(event.getName())) {
        @SuppressWarnings({ "unchecked", "rawtypes" })
        List<EventListener<Event<D>>> list = (List) listeners.get(event.getName());
        list.forEach(e -> e.onEvent(event));
      }
    } finally {
      lock.unlock();
    }
  }
}
