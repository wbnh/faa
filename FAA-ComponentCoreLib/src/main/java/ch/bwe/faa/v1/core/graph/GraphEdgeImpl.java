package ch.bwe.faa.v1.core.graph;

import java.math.BigDecimal;
import java.util.Optional;

import ch.bwe.faa.v1.core.util.AssertUtils;

/**
 * Default implementation of edge.
 * 
 * @author Benjamin Weber
 *
 */
class GraphEdgeImpl<K, V> implements GraphEdge<K, V> {

	private final boolean biDirectional;
	private GraphNode<K, V> source;
	private GraphNode<K, V> target;
	private BigDecimal weight;

	/**
	 * Constructor handling initialization of mandatory fields.
	 *
	 */
	public GraphEdgeImpl(boolean biDirectional, GraphNode<K, V> source, GraphNode<K, V> target, BigDecimal weight) {
		AssertUtils.notNull(target);
		this.biDirectional = biDirectional;
		this.source = source;
		this.target = target;
		this.weight = weight;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isBiDirectional() {
		return biDirectional;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Optional<GraphNode<K, V>> getSource() {
		return Optional.ofNullable(source);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public GraphNode<K, V> getTarget() {
		return target;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public BigDecimal getWeight() {
		return weight;
	}

}
