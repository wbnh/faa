package ch.bwe.faa.v1.core.event;

/**
 * @author benjamin
 * @param <E> The type of event
 */
@FunctionalInterface
public interface EventListener<E extends Event<?>> {

  /**
   * Is fired when an event occurs.
   * 
   * @param event the event that occurred
   */
  void onEvent(E event);
}
