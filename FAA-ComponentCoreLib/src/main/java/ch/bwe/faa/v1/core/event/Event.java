package ch.bwe.faa.v1.core.event;

import java.time.LocalDateTime;

/**
 * Event.
 * 
 * @author benjamin
 * @param <D> The type of the event data
 */
public interface Event<D> {

  /**
   * Gets the name of the event. May serve as string representation.
   * 
   * @return the name
   */
  String getName();

  /**
   * Gets the data given to the event upon instantiation.
   * 
   * @return the data
   */
  D getData();

  /**
   * Gets the timestamp the event occurred on.
   * 
   * @return the date
   */
  LocalDateTime getTimestamp();
}
