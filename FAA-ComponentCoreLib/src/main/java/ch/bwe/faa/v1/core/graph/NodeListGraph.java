package ch.bwe.faa.v1.core.graph;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import ch.bwe.faa.v1.core.util.AssertUtils;

/**
 * Graph implemented using a node list.
 *
 * @author Benjamin Weber
 *
 */
class NodeListGraph<K, V> implements Graph<K, V> {

	private List<GraphNode<K, V>> nodes = new LinkedList<>();

	/**
	 * Constructor handling initialization of mandatory fields.
	 *
	 */
	NodeListGraph(List<GraphNodeImpl<K, V>> nodes) {
		AssertUtils.notNull(nodes);
		this.nodes.addAll(nodes);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<GraphNode<K, V>> getNodes() {
		return new LinkedList<>(nodes);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public GraphNode<K, V> getNode(K key) {
		for (GraphNode<K, V> node : nodes) {
			if (Objects.equals(key, node.getKey())) {
				return node;
			}
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<GraphEdge<K, V>> getEdges() {
		List<GraphEdge<K, V>> edges = new LinkedList<>();

		for (GraphNode<K, V> node : nodes) {
			List<GraphEdge<K, V>> nodeEdges = node.getEdges();
			for (GraphEdge<K, V> edge : nodeEdges) {
				if (!containsEdge(edges, edge)) {
					edges.add(edge);
				}
			}
		}

		return edges;
	}

	private boolean containsEdge(List<GraphEdge<K, V>> edges, GraphEdge<K, V> edge) {
		AssertUtils.notNull(edge);
		AssertUtils.notNull(edges);

		if (edge.isBiDirectional()) {
			for (GraphEdge<K, V> currentEdge : edges) {
				GraphNode<K, V> edgeSource = edge.getSource().get();
				GraphNode<K, V> currentEdgeSource = currentEdge.getSource().get();
				GraphNode<K, V> edgeTarget = edge.getTarget();
				GraphNode<K, V> currentEdgeTarget = currentEdge.getTarget();

				if ((Objects.equals(edgeSource, currentEdgeSource) || Objects.equals(edgeSource, currentEdgeTarget))
								&& (Objects.equals(edgeTarget, currentEdgeSource) || Objects.equals(edgeTarget, currentEdgeTarget))
								&& Objects.equals(edge.getWeight(), currentEdge.getWeight())) {
					return true;
				}
			}
			return false;
		} else {
			for (GraphEdge<K, V> currentEdge : edges) {
				GraphNode<K, V> edgeSource = edge.getSource().get();
				GraphNode<K, V> currentEdgeSource = currentEdge.getSource().get();
				GraphNode<K, V> edgeTarget = edge.getTarget();
				GraphNode<K, V> currentEdgeTarget = currentEdge.getTarget();

				if (Objects.equals(edgeSource, currentEdgeSource) && Objects.equals(edgeTarget, currentEdgeTarget)
								&& Objects.equals(edge.getWeight(), currentEdge.getWeight())) {
					return true;
				}
			}
			return false;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public GraphEdge<K, V> getEdge(K source, K target) {
		return getEdge(source, target, null);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public GraphEdge<K, V> getEdge(K source, K target, BigDecimal weight) {
		GraphNode<K, V> sourceNode = getNode(source);
		AssertUtils.notNull(sourceNode);
		List<GraphEdge<K, V>> edges = sourceNode.getEdges();
		for (GraphEdge<K, V> edge : edges) {
			if (Objects.equals(target, edge.getTarget().getKey()) && Objects.equals(weight, edge.getWeight())) {
				return edge;
			}
		}
		return null;
	}

}
