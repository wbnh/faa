package ch.bwe.faa.v1.core.util;

/**
 * Version type based on major, minor, patch.
 *
 * @author Benjamin Weber
 *
 */
public class Version implements Comparable<Version> {
	private static final char SEPARATOR = '.';
	private static final String NON_DEFINITIVE_INDICATOR = "-SNAPSHOT";

	private final Integer major;
	private final Integer minor;
	private final Integer patch;
	private final boolean definitive;

	public Version(Integer major) {
		this(major, null);
	}

	public Version(Integer major, boolean definitive) {
		this(major, null, definitive);
	}

	public Version(Integer major, Integer minor) {
		this(major, minor, null);
	}

	public Version(Integer major, Integer minor, boolean definitive) {
		this(major, minor, null, definitive);
	}

	public Version(Integer major, Integer minor, Integer patch) {
		this(major, minor, patch, true);
	}

	public Version(Integer major, Integer minor, Integer patch, boolean definitive) {

		AssertUtils.notNull(major, "Major version must not be null.");
		AssertUtils.assertFalse(major != null && minor == null && patch != null,
						"Minor version must be set for patch to be set.");

		this.major = major;
		this.minor = minor;
		this.patch = patch;
		this.definitive = definitive;
	}

	@SuppressWarnings("null")
	public Version(String string) {
		AssertUtils.assertFalse(string == null || string.isEmpty());

		if (string.endsWith(NON_DEFINITIVE_INDICATOR)) {
			definitive = false;
			string = string.substring(0, string.length() - NON_DEFINITIVE_INDICATOR.length());
		} else {
			definitive = true;
		}

		String[] parts = string.split("\\" + SEPARATOR);

		AssertUtils.assertFalse(parts.length == 0);

		if (parts.length > 0) {
			major = Integer.valueOf(parts[0]);
		} else {
			major = null;
		}
		if (parts.length > 1) {
			minor = Integer.valueOf(parts[1]);
		} else {
			minor = null;
		}
		if (parts.length > 2) {
			patch = Integer.valueOf(parts[2]);
		} else {
			patch = null;
		}
	}

	public Integer getMajor() {
		return major;
	}

	public Integer getMinor() {
		return minor;
	}

	public Integer getPatch() {
		return patch;
	}

	public boolean isDefinitive() {
		return definitive;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int compareTo(Version o) {
		// -1 for this < o
		// 0 for this = o
		// 1 for this > o

		if (o == null) {
			return 1;
		}
		if (o == this) {
			return 0;
		}

		int majorCompare = Integer.compare(major, o.major);
		if (majorCompare != 0 || (minor == null && o.minor != null) || (minor != null && o.minor == null)) {
			return majorCompare;
		}

		int minorCompare = Integer.compare(minor, o.minor);
		if (minorCompare != 0 || (patch == null && o.patch != null) || (patch != null && o.patch == null)) {
			return minorCompare;
		}

		int patchCompare = Integer.compare(patch, o.patch);
		if (patchCompare != 0) {
			return patchCompare;
		}

		if (definitive && !o.definitive) {
			return 1;
		} else if (!definitive && o.definitive) {
			return -1;
		} else {
			return 0;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();

		builder.append(major);
		if (minor != null) {
			builder.append(SEPARATOR);
			builder.append(minor);
			if (patch != null) {
				builder.append(SEPARATOR);
				builder.append(patch);
			}
		}
		if (!definitive) {
			builder.append(NON_DEFINITIVE_INDICATOR);
		}

		return builder.toString();
	}
}
