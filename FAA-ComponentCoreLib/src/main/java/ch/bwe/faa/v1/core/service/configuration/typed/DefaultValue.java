package ch.bwe.faa.v1.core.service.configuration.typed;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author benjamin
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface DefaultValue {

	/**
	 * The default value.
	 * 
	 * @return the value
	 */
	String value();
}
