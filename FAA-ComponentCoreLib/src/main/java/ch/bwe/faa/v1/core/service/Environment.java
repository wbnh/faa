package ch.bwe.faa.v1.core.service;

/**
 * The different environments the application can run in.
 * 
 * @author benjamin
 */
public enum Environment {

  /**
   * Development
   */
  DEV,

  /**
   * Integration
   */
  INT,

  /**
   * Test
   */
  TST,

  /**
   * Production
   */
  PRD;

  /**
   * All environments
   */
  public static final Environment[] ALL = { DEV, INT, TST, PRD };

  /**
   * Non-productive environments
   */
  public static final Environment[] INDEV = { DEV, INT, TST };
}
