package ch.bwe.faa.v1.core.service.configuration.typed;

import java.io.Serializable;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Proxy;
import java.lang.reflect.Type;
import java.text.MessageFormat;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.faa.v1.core.service.configuration.Configuration;
import ch.bwe.faa.v1.core.service.configuration.ConfigurationImpl;
import ch.bwe.faa.v1.core.service.configuration.ConfigurationProperties;
import ch.bwe.faa.v1.core.util.AssertUtils;

/**
 * @author benjamin
 */
public class TypedConfigurationProxyImpl implements TypedConfigurationProxy {

  private static final long serialVersionUID = 1628775163019616475L;

  /**
   * @author benjamin
   * @param <T>
   * @param <U>
   */
  private final class ConfigurationProxyInvocationHandler<T, U> implements InvocationHandler, Serializable {

    private static final long serialVersionUID = -4087291886312956053L;
    private Configuration configuration;

    /**
     * Constructor.
     * 
     * @param properties the properties to use
     */
    public ConfigurationProxyInvocationHandler(ConfigurationProperties<T> properties) {
      super();

      configuration = ServiceRegistry.getConfigurationProxy().getConfiguration(properties);

      AssertUtils.notNull(configuration,
          "The configuration file was not found for configuration '" + properties.getConfiguration() + "', context '"
              + properties.getContext() + "' and variant '" + properties.getVariantId() + "'");
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

      Class<?> returnType = method.getReturnType();
      Object returnObject = null;

      if (returnType.isAssignableFrom(String.class)) {
        String stringValue = configuration.getStringValue(method.getName());

        if (method.isAnnotationPresent(DefaultValue.class)) {
          if (stringValue == null) {
            stringValue = method.getAnnotation(DefaultValue.class).value();
          }
        }
        if (stringValue == null) {
          stringValue = "";
        }

        returnObject = MessageFormat.format(stringValue, args);

      } else if (returnType.isAssignableFrom(Boolean.class)) {
        returnObject = configuration.getBooleanValue(method.getName());

        if (method.isAnnotationPresent(DefaultValue.class)) {
          if (returnObject == null) {
            returnObject = Boolean.valueOf(method.getAnnotation(DefaultValue.class).value());
          }
        }
      } else if (returnType.isAssignableFrom(Short.class)) {
        returnObject = configuration.getShortValue(method.getName());

        if (method.isAnnotationPresent(DefaultValue.class)) {
          if (returnObject == null) {
            returnObject = Short.valueOf(method.getAnnotation(DefaultValue.class).value());
          }
        }
      } else if (returnType.isAssignableFrom(Integer.class)) {
        returnObject = configuration.getIntegerValue(method.getName());

        if (method.isAnnotationPresent(DefaultValue.class)) {
          if (returnObject == null) {
            returnObject = Integer.valueOf(method.getAnnotation(DefaultValue.class).value());
          }
        }
      } else if (returnType.isAssignableFrom(Long.class)) {
        returnObject = configuration.getLongValue(method.getName());

        if (method.isAnnotationPresent(DefaultValue.class)) {
          if (returnObject == null) {
            returnObject = Long.valueOf(method.getAnnotation(DefaultValue.class).value());
          }
        }
      } else if (returnType.isAssignableFrom(Float.class)) {
        returnObject = configuration.getFloatValue(method.getName());

        if (method.isAnnotationPresent(DefaultValue.class)) {
          if (returnObject == null) {
            returnObject = Float.valueOf(method.getAnnotation(DefaultValue.class).value());
          }
        }
      } else if (returnType.isAssignableFrom(Double.class)) {
        returnObject = configuration.getDoubleValue(method.getName());

        if (method.isAnnotationPresent(DefaultValue.class)) {
          if (returnObject == null) {
            returnObject = Double.valueOf(method.getAnnotation(DefaultValue.class).value());
          }
        }
      } else if (returnType.isAssignableFrom(List.class)) {
        Type genericReturnType = method.getGenericReturnType();
        if (genericReturnType instanceof ParameterizedType) {
          Type listType = ((ParameterizedType) genericReturnType).getActualTypeArguments()[0];
          if (listType instanceof Class<?>) {
            returnType = (Class<?>) listType;
            if (returnType.isAssignableFrom(String.class)) {
              returnObject = configuration.getStringList(method.getName());

              if (method.isAnnotationPresent(DefaultValue.class)) {
                if (returnObject == null) {
                  List<String> list = new LinkedList<>();
                  Collections.addAll(list,
                      method.getAnnotation(DefaultValue.class).value().split(ConfigurationImpl.LIST_DELIM));
                  returnObject = list;
                }
              }
            } else if (returnType.isAssignableFrom(Boolean.class)) {
              returnObject = configuration.getBooleanList(method.getName());

              if (method.isAnnotationPresent(DefaultValue.class)) {
                if (returnObject == null) {
                  List<Boolean> list = new LinkedList<>();
                  for (String currentString : method.getAnnotation(DefaultValue.class).value()
                      .split(ConfigurationImpl.LIST_DELIM))
                    list.add(Boolean.valueOf(currentString));
                  returnObject = list;
                }
              }
            } else if (returnType.isAssignableFrom(Short.class)) {
              returnObject = configuration.getShortList(method.getName());

              if (method.isAnnotationPresent(DefaultValue.class)) {
                if (returnObject == null) {
                  List<Short> list = new LinkedList<>();
                  for (String currentString : method.getAnnotation(DefaultValue.class).value()
                      .split(ConfigurationImpl.LIST_DELIM))
                    list.add(Short.valueOf(currentString));
                  returnObject = list;
                }
              }
            } else if (returnType.isAssignableFrom(Integer.class)) {
              returnObject = configuration.getIntegerList(method.getName());

              if (method.isAnnotationPresent(DefaultValue.class)) {
                if (returnObject == null) {
                  List<Integer> list = new LinkedList<>();
                  for (String currentString : method.getAnnotation(DefaultValue.class).value()
                      .split(ConfigurationImpl.LIST_DELIM))
                    list.add(Integer.valueOf(currentString));
                  returnObject = list;
                }
              }
            } else if (returnType.isAssignableFrom(Long.class)) {
              returnObject = configuration.getLongList(method.getName());

              if (method.isAnnotationPresent(DefaultValue.class)) {
                if (returnObject == null) {
                  List<Long> list = new LinkedList<>();
                  for (String currentString : method.getAnnotation(DefaultValue.class).value()
                      .split(ConfigurationImpl.LIST_DELIM))
                    list.add(Long.valueOf(currentString));
                  returnObject = list;
                }
              }
            } else if (returnType.isAssignableFrom(Float.class)) {
              returnObject = configuration.getFloatList(method.getName());

              if (method.isAnnotationPresent(DefaultValue.class)) {
                if (returnObject == null) {
                  List<Float> list = new LinkedList<>();
                  for (String currentString : method.getAnnotation(DefaultValue.class).value()
                      .split(ConfigurationImpl.LIST_DELIM))
                    list.add(Float.valueOf(currentString));
                  returnObject = list;
                }
              }
            } else if (returnType.isAssignableFrom(Double.class)) {
              returnObject = configuration.getDoubleList(method.getName());

              if (method.isAnnotationPresent(DefaultValue.class)) {
                if (returnObject == null) {
                  List<Double> list = new LinkedList<>();
                  for (String currentString : method.getAnnotation(DefaultValue.class).value()
                      .split(ConfigurationImpl.LIST_DELIM))
                    list.add(Double.valueOf(currentString));
                  returnObject = list;
                }
              }
            }
          }
        }
      }

      return returnObject;
    }
  }

  private Map<Class<?>, Object> configs = new HashMap<>();

  /**
   * Constructor.
   */
  public TypedConfigurationProxyImpl() {
    super();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public <T> T get(Class<T> config) {
    return get(config, config);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public <T, U> T get(Class<T> config, Class<U> context) {
    return get(config, context, null);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public <T> T get(Class<T> config, String variantId) {
    return get(config, config, variantId);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public <T, U> T get(Class<T> config, Class<U> context, String variantId) {
    return get(new ConfigurationProperties<>(config).setContext(context).setVariantId(variantId));
  }

  /**
   * {@inheritDoc}
   */
  @SuppressWarnings("unchecked")
  @Override
  public <T> T get(ConfigurationProperties<T> properties) {

    if (configs.get(properties.getConfiguration()) == null) {
      Object loadedConfig = Proxy.newProxyInstance(properties.getContext().getClassLoader(),
          new Class<?>[] { properties.getConfiguration() }, new ConfigurationProxyInvocationHandler<>(properties));
      AssertUtils.notNull(loadedConfig, "Could not load requested configuration");
      configs.put(properties.getConfiguration(), loadedConfig);
    }

    return (T) configs.get(properties.getConfiguration());
  }

}
