package ch.bwe.faa.v1.core.graph;

import java.util.List;

/**
 * A certain node within a graph.
 *
 * @author Benjamin Weber
 *
 */
public interface GraphNode<K, V> {

	void setKey(K key);

	K getKey();

	void setValue(V value);

	V getValue();

	List<GraphEdge<K, V>> getEdges();
}
