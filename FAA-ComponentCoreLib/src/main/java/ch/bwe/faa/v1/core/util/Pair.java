package ch.bwe.faa.v1.core.util;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author benjamin
 * @param <A> The first type
 * @param <B> The second type
 */
public class Pair<A, B> implements Serializable {

  private static final long serialVersionUID = -7063526371108874890L;
  private A a;
  private B b;

  /**
   * Constructor.
   */
  public Pair() {
    this(null, null);
  }

  /**
   * Constructor.
   * 
   * @param a the first object
   * @param b the first object
   */
  public Pair(A a, B b) {
    super();

    this.a = a;
    this.b = b;
  }

  /**
   * @return the a
   */
  public A getA() {
    return a;
  }

  /**
   * @param a the a to set
   */
  public void setA(A a) {
    this.a = a;
  }

  /**
   * @return the b
   */
  public B getB() {
    return b;
  }

  /**
   * @param b the b to set
   */
  public void setB(B b) {
    this.b = b;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public int hashCode() {
    return (a != null ? a.hashCode() * 5 : 5) + (b != null ? b.hashCode() * 7 : 7);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean equals(Object obj) {
    if (obj == null) { return false; }
    if (!(obj instanceof Pair)) { return false; }
    Pair<?, ?> pair = (Pair<?, ?>) obj;
    return Objects.equals(a, pair.a) && Objects.equals(b, pair.b);
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();

    builder.append("(");
    builder.append(Objects.toString(a));
    builder.append(", ");
    builder.append(Objects.toString(b));
    builder.append(")");

    return builder.toString();
  }
}
