package ch.bwe.faa.v1.core.command;

import java.util.HashMap;
import java.util.Map;

import ch.bwe.faa.v1.core.util.AssertUtils;

/**
 * Registry for different commands.
 *
 * @author Benjamin Weber
 *
 */
public class CommandRegistry implements CommandExecutor {

	private Map<String, AbstractNamedCommand> commands = new HashMap<>();

	/**
	 * Constructor handling initialization of mandatory fields.
	 *
	 */
	public CommandRegistry() {
	}

	public AbstractNamedCommand addCommand(String name, Command command) {
		NamedCommandWrapper wrapper = new NamedCommandWrapper(name, command);

		addCommand(wrapper);

		return wrapper;
	}

	public void addCommand(AbstractNamedCommand command) {
		AssertUtils.notNull(command);

		commands.put(command.getName(), command);
	}

	public CommandExecutionResult executeCommand(String name) {
		AssertUtils.notNull(name);

		return executeCommand(commands.get(name));
	}

}
