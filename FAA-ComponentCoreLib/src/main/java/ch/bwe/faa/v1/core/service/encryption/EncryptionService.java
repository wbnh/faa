package ch.bwe.faa.v1.core.service.encryption;

import java.security.Security;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;

/**
 * Utility for encrypting and decrypting data.
 *
 * @author Benjamin Weber
 *
 */
public class EncryptionService {
	static {
		Security.addProvider(new BouncyCastleProvider());
	}
	private static final String BOUNCY_CASTLE_PROVIDER_NAME = "BC";
	private static final String PBE_ALGORITHM_NAME = "PBEWITHSHA256AND128BITAES-CBC-BC";

	/**
	 * Constructor handling initialization of mandatory fields.
	 *
	 */
	public EncryptionService() {
	}

	public StandardPBEStringEncryptor createPbeStringEncryptor(String password) {
		StandardPBEStringEncryptor pbeStringEncryptor = new StandardPBEStringEncryptor();
		pbeStringEncryptor.setProviderName(BOUNCY_CASTLE_PROVIDER_NAME);
		pbeStringEncryptor.setAlgorithm(PBE_ALGORITHM_NAME);
		pbeStringEncryptor.setStringOutputType(StandardPBEStringEncryptor.DEFAULT_STRING_OUTPUT_TYPE);
		pbeStringEncryptor.setPassword(password);
		return pbeStringEncryptor;
	}

	public String encryptString(String password, String input) {
		return createPbeStringEncryptor(password).encrypt(input);
	}

	public String decryptString(String password, String input) {
		return createPbeStringEncryptor(password).decrypt(input);
	}
}
