package ch.bwe.faa.v1.test.configuration;

import org.junit.Assert;
import org.junit.Test;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.faa.v1.core.service.configuration.Configuration;
import ch.bwe.faa.v1.core.service.configuration.ConfigurationProperties;

public class TestClassConfigurationProxy {

	private static final String STRING_KEY = "testString";
	private static final String BOOLEAN_KEY = "testBoolean";
	private static final String SHORT_KEY = "testShort";
	private static final String INTEGER_KEY = "testInteger";
	private static final String LONG_KEY = "testLong";
	private static final String FLOAT_KEY = "testFloat";
	private static final String DOUBLE_KEY = "testDouble";

	private static final String STRING_LIST_KEY = "testStringList";
	private static final String BOOLEAN_LIST_KEY = "testBooleanList";
	private static final String SHORT_LIST_KEY = "testShortList";
	private static final String INTEGER_LIST_KEY = "testIntegerList";
	private static final String LONG_LIST_KEY = "testLongList";
	private static final String FLOAT_LIST_KEY = "testFloatList";
	private static final String DOUBLE_LIST_KEY = "testDoubleList";

	@Test
	public void testLoad() throws Exception {
		Configuration configuration = ServiceRegistry.getConfigurationProxy()
						.getConfiguration(new ConfigurationProperties<>(getClass()));

		Assert.assertEquals("testStringValue", configuration.getStringValue(STRING_KEY));
		Assert.assertEquals(Boolean.TRUE, configuration.getBooleanValue(BOOLEAN_KEY));
		Assert.assertEquals(Short.valueOf((short) 11), configuration.getShortValue(SHORT_KEY));
		Assert.assertEquals(Integer.valueOf(111), configuration.getIntegerValue(INTEGER_KEY));
		Assert.assertEquals(Long.valueOf(1111L), configuration.getLongValue(LONG_KEY));
		Assert.assertEquals(Float.valueOf(1.0f), configuration.getFloatValue(FLOAT_KEY));
		Assert.assertEquals(Double.valueOf(12.8), configuration.getDoubleValue(DOUBLE_KEY));

		Assert.assertArrayEquals(new String[] { "value1", "value2", "value3" },
						configuration.getStringList(STRING_LIST_KEY).toArray());
		Assert.assertArrayEquals(new Boolean[] { true, false, true },
						configuration.getBooleanList(BOOLEAN_LIST_KEY).toArray());
		Assert.assertArrayEquals(new Short[] { 11, 12, 13 }, configuration.getShortList(SHORT_LIST_KEY).toArray());
		Assert.assertArrayEquals(new Integer[] { 111, 112, 113 }, configuration.getIntegerList(INTEGER_LIST_KEY).toArray());
		Assert.assertArrayEquals(new Long[] { 1111L, 1112L, 1113L }, configuration.getLongList(LONG_LIST_KEY).toArray());
		Assert.assertArrayEquals(new Float[] { 1.0f, 1.1f, 1.2f }, configuration.getFloatList(FLOAT_LIST_KEY).toArray());
		Assert.assertArrayEquals(new Double[] { 12.8, 12.9, 13.0 }, configuration.getDoubleList(DOUBLE_LIST_KEY).toArray());
	}
}
