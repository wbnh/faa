package ch.bwe.faa.v1.test.configuration;

import java.util.List;

import ch.bwe.faa.v1.core.service.configuration.typed.DefaultValue;

/**
 * @author benjamin
 *
 */
public interface TypedConfiguration {

	String stringValue();

	@DefaultValue("testStringAnnotated")
	String stringValueAnnotated();

	Boolean booleanValue();

	@DefaultValue("false")
	Boolean booleanValueAnnotated();

	Short shortValue();

	@DefaultValue("20")
	Short shortValueAnnotated();

	Integer integerValue();

	@DefaultValue("200")
	Integer integerValueAnnotated();

	Long longValue();

	@DefaultValue("2000")
	Long longValueAnnotated();

	Float floatValue();

	@DefaultValue("22.8")
	Float floatValueAnnotated();

	Double doubleValue();

	@DefaultValue("25.6")
	Double doubleValueAnnotated();

	List<String> stringListValue();

	@DefaultValue("firstAnnotated;secondAnnotated;thirdAnnotated")
	List<String> stringListValueAnnotated();

	List<Boolean> booleanListValue();

	@DefaultValue("true;false;true")
	List<Boolean> booleanListValueAnnotated();

	List<Short> shortListValue();

	@DefaultValue("20;21;22")
	List<Short> shortListValueAnnotated();

	List<Integer> integerListValue();

	@DefaultValue("200;201;202")
	List<Integer> integerListValueAnnotated();

	List<Long> longListValue();

	@DefaultValue("2000;2001;2002")
	List<Long> longListValueAnnotated();

	List<Float> floatListValue();

	@DefaultValue("22.8;22.9;23.0")
	List<Float> floatListValueAnnotated();

	List<Double> doubleListValue();

	@DefaultValue("25.6;25.7;25.8")
	List<Double> doubleListValueAnnotated();
}
