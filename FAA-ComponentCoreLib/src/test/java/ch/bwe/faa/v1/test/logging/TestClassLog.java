package ch.bwe.faa.v1.test.logging;

import org.junit.Test;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.faa.v1.core.service.logging.LogProxy;

public class TestClassLog {

	@Test
	public void testLog() throws Exception {
		LogProxy logProxy = ServiceRegistry.getLogProxy();

		final String message = "logMessage";

		logProxy.trace(this, message);
		logProxy.debug(this, message);
		logProxy.info(this, message);
		logProxy.warn(this, message, null);
		logProxy.error(this, message, null);
		logProxy.fatal(this, message, new IllegalArgumentException(
				new NullPointerException()));
	}

	@Test(expected = AssertionError.class)
	public void testLogFatalRequireThrowable() {
		ServiceRegistry.getLogProxy().fatal(this, "Message", null);
	}
}
