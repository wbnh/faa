package ch.bwe.faa.v1.test.graph;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Test;

import ch.bwe.faa.v1.core.graph.Graph;
import ch.bwe.faa.v1.core.graph.GraphBuilder;
import ch.bwe.faa.v1.core.graph.GraphTraversal;

/**
 * Class to test the dijkstra algorithm.
 *
 * @author Benjamin Weber
 *
 */
public class TestClassDijkstra {

	@Test
	public void testSuccessful() throws Exception {
		final BigDecimal BASEL_BERN = BigDecimal.valueOf(75L);
		final BigDecimal BASEL_WINTERTHUR = BigDecimal.valueOf(125L);
		final BigDecimal BERN_FLUGHAFEN = BigDecimal.valueOf(100L);

		GraphBuilder<String, String> builder = new GraphBuilder<>();
		builder.node("Basel").node("Zürich").node("Winterthur").node("Olten").node("Bern").node("Flughafen")
						.node("Bassersdorf");
		builder.edge("Basel", "Olten", BigDecimal.valueOf(50L)).edge("Olten", "Bern", BigDecimal.valueOf(25L))
						.edge("Olten", "Zürich", BigDecimal.valueOf(50L)).edge("Zürich", "Flughafen", BigDecimal.valueOf(25L))
						.edge("Zürich", "Bassersdorf", BigDecimal.valueOf(12L))
						.edge("Flughafen", "Winterthur", BigDecimal.valueOf(25L))
						.edge("Bassersdorf", "Winterthur", BigDecimal.valueOf(13L));
		Graph<String, String> graph = builder.build();

		GraphTraversal<String, String> baselBern = graph.dijkstra("Basel", "Bern");
		System.out.println(baselBern);
		Assert.assertEquals(BASEL_BERN, baselBern.getRemainingWeight());

		GraphTraversal<String, String> baselWinterthur = graph.dijkstra("Basel", "Winterthur");
		System.out.println(baselWinterthur);
		Assert.assertEquals(BASEL_WINTERTHUR, baselWinterthur.getRemainingWeight());

		GraphTraversal<String, String> bernFlughafen = graph.dijkstra("Bern", "Flughafen");
		System.out.println(bernFlughafen);
		Assert.assertEquals(BERN_FLUGHAFEN, bernFlughafen.getRemainingWeight());
	}
}
