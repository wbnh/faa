package ch.bwe.faa.v1.test.configuration;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;

import org.junit.Test;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.faa.v1.core.service.configuration.ConfigurationProperties;

/**
 * @author benjamin
 *
 */
public class TestClassTypedConfigurationProxy {

	@Test
	public void testLoad() throws Exception {
		TypedConfiguration typedConfiguration = ServiceRegistry.getTypedConfigurationProxy()
						.get(new ConfigurationProperties<>(TypedConfiguration.class));

		assertEquals("testString", typedConfiguration.stringValue());
		assertEquals("testStringAnnotated", typedConfiguration.stringValueAnnotated());
		assertEquals(true, typedConfiguration.booleanValue());
		assertEquals(false, typedConfiguration.booleanValueAnnotated());
		assertEquals(Short.valueOf("10"), typedConfiguration.shortValue());
		assertEquals(Short.valueOf("20"), typedConfiguration.shortValueAnnotated());
		assertEquals(Integer.valueOf(100), typedConfiguration.integerValue());
		assertEquals(Integer.valueOf(200), typedConfiguration.integerValueAnnotated());
		assertEquals(Long.valueOf(1000L), typedConfiguration.longValue());
		assertEquals(Long.valueOf(2000L), typedConfiguration.longValueAnnotated());
		assertEquals(Float.valueOf(12.8f), typedConfiguration.floatValue());
		assertEquals(Float.valueOf(22.8f), typedConfiguration.floatValueAnnotated());
		assertEquals(Double.valueOf(15.6), typedConfiguration.doubleValue());
		assertEquals(Double.valueOf(25.6), typedConfiguration.doubleValueAnnotated());
		assertEquals(Arrays.asList("first", "second", "third"), typedConfiguration.stringListValue());
		assertEquals(Arrays.asList("firstAnnotated", "secondAnnotated", "thirdAnnotated"),
						typedConfiguration.stringListValueAnnotated());
		assertEquals(Arrays.asList(false, true, false), typedConfiguration.booleanListValue());
		assertEquals(Arrays.asList(true, false, true), typedConfiguration.booleanListValueAnnotated());
		assertEquals(Arrays.asList(Short.valueOf("10"), Short.valueOf("11"), Short.valueOf("12")),
						typedConfiguration.shortListValue());
		assertEquals(Arrays.asList(Short.valueOf("20"), Short.valueOf("21"), Short.valueOf("22")),
						typedConfiguration.shortListValueAnnotated());
		assertEquals(Arrays.asList(Integer.valueOf(100), Integer.valueOf(101), Integer.valueOf(102)),
						typedConfiguration.integerListValue());
		assertEquals(Arrays.asList(Integer.valueOf(200), Integer.valueOf(201), Integer.valueOf(202)),
						typedConfiguration.integerListValueAnnotated());
		assertEquals(Arrays.asList(Long.valueOf(1000), Long.valueOf(1001), Long.valueOf(1002)),
						typedConfiguration.longListValue());
		assertEquals(Arrays.asList(Long.valueOf(2000), Long.valueOf(2001), Long.valueOf(2002)),
						typedConfiguration.longListValueAnnotated());
		assertEquals(Arrays.asList(Float.valueOf(12.8f), Float.valueOf(12.9f), Float.valueOf(13.0f)),
						typedConfiguration.floatListValue());
		assertEquals(Arrays.asList(Float.valueOf(22.8f), Float.valueOf(22.9f), Float.valueOf(23.0f)),
						typedConfiguration.floatListValueAnnotated());
		assertEquals(Arrays.asList(Double.valueOf(15.6), Double.valueOf(15.7), Double.valueOf(15.8)),
						typedConfiguration.doubleListValue());
		assertEquals(Arrays.asList(Double.valueOf(25.6), Double.valueOf(25.7), Double.valueOf(25.8)),
						typedConfiguration.doubleListValueAnnotated());
	}
}
