package ch.bwe.faa.v1.test.encryption;

import org.junit.Assert;
import org.junit.Test;

import ch.bwe.faa.v1.core.service.ServiceRegistry;

/**
 * Test class to test the encryption service.
 *
 * @author Benjamin Weber
 *
 */
public class TestClassEncryptionService {

	private static final String PASSWORD = "stringPassword";

	@Test
	public void testEncryptDecryptString() throws Exception {
		final String input = "input string for encryption";

		String encrypted = ServiceRegistry.getEncryptionService().encryptString(PASSWORD, input);
		Assert.assertEquals(input, ServiceRegistry.getEncryptionService().decryptString(PASSWORD, encrypted));
	}
}
