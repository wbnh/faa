package ch.bwe.faa.v1.test.util;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import ch.bwe.faa.v1.core.util.StringUtils;

/**
 * Tests the string utils.
 * 
 * @author benjamin
 */
public class TestClassStringUtils {

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testStringToType() throws Exception {
    String input;
    Object expected;

    input = "true";
    expected = true;
    Assert.assertEquals(expected, StringUtils.convertStringToType(Boolean.class, input));

    input = "100";
    expected = Byte.valueOf((byte) 100);
    Assert.assertEquals(expected, StringUtils.convertStringToType(Byte.class, input));

    input = "1";
    expected = Short.valueOf((short) 1);
    Assert.assertEquals(expected, StringUtils.convertStringToType(Short.class, input));

    input = "2";
    expected = 2;
    Assert.assertEquals(expected, StringUtils.convertStringToType(Integer.class, input));

    input = "3";
    expected = 3l;
    Assert.assertEquals(expected, StringUtils.convertStringToType(Long.class, input));

    input = "4";
    expected = 4f;
    Assert.assertEquals(expected, StringUtils.convertStringToType(Float.class, input));

    input = "5";
    expected = 5d;
    Assert.assertEquals(expected, StringUtils.convertStringToType(Double.class, input));

    input = "6";
    expected = "6";
    Assert.assertEquals(expected, StringUtils.convertStringToType(String.class, input));

    input = "1;2;3;4;5;6";
    expected = List.of("1", "2", "3", "4", "5", "6");
    Assert.assertEquals(expected, StringUtils.convertStringToList(String.class, input));

    input = "1;2;3;4;5;6";
    expected = List.of(1, 2, 3, 4, 5, 6);
    Assert.assertEquals(expected, StringUtils.convertStringToList(Integer.class, input));
  }
}
