package ch.bwe.faa.v1.test.logging;

import org.junit.Test;

import ch.bwe.faa.v1.core.util.AssertUtils;

public class TestClassAssert {

	@Test
	public void testNullP() throws Exception {
		AssertUtils.isNull(null);
	}

	@Test(expected = AssertionError.class)
	public void testNullN() throws Exception {
		AssertUtils.isNull("");
	}

	@Test
	public void testNotNullP() throws Exception {
		AssertUtils.notNull("");
	}

	@Test(expected = AssertionError.class)
	public void getNotNullN() throws Exception {
		AssertUtils.notNull(null);
	}

	@Test
	public void testTruePrimitiveP() throws Exception {
		AssertUtils.assertTrue(true);
	}

	@Test(expected = AssertionError.class)
	public void testTruePrimitiveN() throws Exception {
		AssertUtils.assertTrue(false);
	}

	@Test
	public void testTrueWrapperP() throws Exception {
		AssertUtils.assertTrue(Boolean.TRUE);
	}

	@Test(expected = AssertionError.class)
	public void testTrueWrapperN() throws Exception {
		AssertUtils.assertTrue(Boolean.FALSE);
	}

	@Test
	public void testFalsePrimitiveP() throws Exception {
		AssertUtils.assertFalse(false);
	}

	@Test(expected = AssertionError.class)
	public void testFalsePrimitiveN() throws Exception {
		AssertUtils.assertFalse(true);
	}

	@Test
	public void testFalseWrapperP() throws Exception {
		AssertUtils.assertFalse(Boolean.FALSE);
	}

	@Test(expected = AssertionError.class)
	public void testFalseWrapperN() throws Exception {
		AssertUtils.assertFalse(Boolean.TRUE);
	}

	@Test
	public void testEqualsPN() throws Exception {
		AssertUtils.assertEquals(null, null);
	}

	@Test
	public void testEqualsPNN() throws Exception {
		AssertUtils.assertEquals(Integer.valueOf(0), Integer.valueOf(0));
	}

	@Test(expected = AssertionError.class)
	public void testEqualsN() throws Exception {
		AssertUtils.assertEquals(Integer.valueOf(0), Integer.valueOf(1));
	}

	@Test
	public void testNotEqualsP() throws Exception {
		AssertUtils.assertNotEquals(Integer.valueOf(0), Integer.valueOf(1));
	}

	@Test(expected = AssertionError.class)
	public void testNotEqualsNN() throws Exception {
		AssertUtils.assertNotEquals(null, null);
	}

	@Test(expected = AssertionError.class)
	public void testNotEqualsNNN() throws Exception {
		AssertUtils.assertNotEquals(Integer.valueOf(0), Integer.valueOf(0));
	}

	@Test
	public void testSamePN() throws Exception {
		AssertUtils.assertSame(null, null);
	}

	@Test
	public void testSamePNN() throws Exception {
		Integer i = Integer.valueOf(0);
		AssertUtils.assertSame(i, i);
	}

	@Test(expected = AssertionError.class)
	public void testSameN() throws Exception {
		AssertUtils.assertSame(Integer.valueOf(0), Integer.valueOf(1));
	}

	@Test
	public void testNotSameP() throws Exception {
		AssertUtils.assertNotSame(Integer.valueOf(0), Integer.valueOf(1));
	}

	@Test(expected = AssertionError.class)
	public void testNotSameNN() throws Exception {
		AssertUtils.assertNotSame(null, null);
	}

	@Test(expected = AssertionError.class)
	public void testNotSameNNN() throws Exception {
		Integer i = Integer.valueOf(0);
		AssertUtils.assertNotSame(i, i);
	}
}
