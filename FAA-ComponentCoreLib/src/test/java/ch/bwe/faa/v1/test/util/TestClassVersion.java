package ch.bwe.faa.v1.test.util;

import org.junit.Assert;
import org.junit.Test;

import ch.bwe.faa.v1.core.util.Version;

/**
 * TestClass for version.
 *
 * @author Benjamin Weber
 *
 */
public class TestClassVersion {

	@Test
	public void testParse() throws Exception {

		String string = "0.0.0";
		Assert.assertEquals(string, new Version(string).toString());

		string = "0.0";
		Assert.assertEquals(string, new Version(string).toString());

		string = "0";
		Assert.assertEquals(string, new Version(string).toString());

		string = "0.0.0-SNAPSHOT";
		Assert.assertEquals(string, new Version(string).toString());

		string = "0.0-SNAPSHOT";
		Assert.assertEquals(string, new Version(string).toString());

		string = "0-SNAPSHOT";
		Assert.assertEquals(string, new Version(string).toString());
	}
}
